import jwt = require("jsonwebtoken");
const JWT_SECRET = process.env.JWT_SECRET;

export interface UserJWTInfo {
  id: string;
  deviceId: string;
}

export interface AdminJWTInfo {
  id: string;
}

export function makeJwtToken(user: UserJWTInfo) {
  const { id, deviceId } = user;

  if (!id || !deviceId) return undefined;

  const data = {
    id,
    deviceId,
  };
  const token = jwt.sign(data, JWT_SECRET);

  return token;
}

export function makeAdminJwtToken(admin: AdminJWTInfo) {
  const { id } = admin;

  if (!id) return undefined;

  const data = {
    id,
  };
  const token = jwt.sign(data, JWT_SECRET);

  return token;
}