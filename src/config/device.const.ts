export enum DEVICE_TYPES {
  iPhone = "iPhone",
  iPad = "iPad",
}

export enum PRESENCE_STATUS {
  online = "online",
  offline = "offline",
}