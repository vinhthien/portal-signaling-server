import _findIndex from "lodash/findIndex";
import _find from "lodash/find";
import _filter from "lodash/filter";
import { makeRandomNumericString } from "../config/utils";
import Device from "../models/device";
import RoomMember from "../models/room-member";
import Room from "../models/room";
import { ROOM_MEMBER_STATUS } from "./room-member.const";
import { PRESENCE_STATUS } from "./device.const";
import { forEach } from "async";


export async function inviteRoom(io, roomId, myPortalId, partnerPortalId) {
  const room = await Room.findRoom(roomId);
  console.log(4343434);
  console.log(room);
  if (room === null || room === undefined) {
    // This room id NOT already exists
    return;
  }

  // Get caller information
  const myDevice = await Device.findDevice(myPortalId);
  if (myDevice === null || myDevice === undefined || myDevice.presenceStatus === PRESENCE_STATUS.offline) {
    // These caller devices NOT EXIST
    return;
  }

  // Get callee information
  const partnerDevice = await Device.findDevice(partnerPortalId);
  if (partnerDevice === null || partnerDevice === undefined || partnerDevice.presenceStatus === PRESENCE_STATUS.offline) {
    // These callee devices NOT EXIST
    return;
  }

  io.to(partnerDevice.socketId).emit("incomingCallReceived", partnerDevice.portalId, {
    room_id: roomId,
    caller_portal_id: myDevice.portalId
  });

}

export async function roomMatched(myPortalId, partnerPortalId) {
  const myDevice = await Device.findDevice(myPortalId);
  const partnerDevice = await Device.findDevice(partnerPortalId);

  const myRoomMembers = await RoomMember.findRoomMembers(myDevice);
  myRoomMembers.forEach(async function (myRoomMember) {
    const partnerRoomMember = await RoomMember.findRoomMember(myRoomMember.roomId, partnerDevice);
    if (myRoomMember.roomId == partnerRoomMember.roomId) {
      return myRoomMember.roomId;
    }
  });

  return null;
}

export async function createNewRoom(io, myPortalId, partnerPortalId) {
  const roomId = `${makeRandomNumericString(8)}`;

  const room = await Room.findRoom(roomId);
  if (room !== null && room !== undefined) {
      // This room id already exists
    return;
  }
  const myDevice = await Device.findDevice(myPortalId);
  const partnerDevice = await Device.findDevice(partnerPortalId);
  await RoomMember.makeRoomMembers(roomId, myDevice, partnerDevice);

  return roomId;
}

export async function joinRoom(io, roomId, myPortalId, partnerPortalId) {
  const room = await Room.findRoom(roomId);
  if (room === null || room === undefined) {
    // This room id NOT already exists
    return;
  }

  const myDevice = await Device.findDevice(myPortalId);
  if (myDevice === undefined || myDevice === null || myDevice.presenceStatus === PRESENCE_STATUS.offline) {
    return;
  }

  const partnerDevice = await Device.findDevice(partnerPortalId);
  if (partnerDevice === undefined || partnerDevice === null || partnerDevice.presenceStatus === PRESENCE_STATUS.offline) {
    return;
  }

  const partnerRoomMember = await RoomMember.findRoomMember(room.roomId, partnerDevice);
  if (partnerRoomMember === undefined || partnerRoomMember === null) {
    return;
  }

  let myRoomMember = await RoomMember.findRoomMember(room.roomId, myDevice);
  if (myRoomMember === undefined || myRoomMember === null) { // Ensure not add twice the device.
    myRoomMember = await RoomMember.createRoomMember(roomId, myDevice, ROOM_MEMBER_STATUS.joined);
  }

  if (myRoomMember.status !== ROOM_MEMBER_STATUS.joined
    || partnerRoomMember.status !== ROOM_MEMBER_STATUS.joined) {
    return;
  }

  io.to(myDevice.socketId).emit("readyToCreateSession", myDevice.portalId, {
    room_id: roomId,
    is_initiator: true
  });

  io.to(partnerDevice.socketId).emit("readyToCreateSession", partnerDevice.portalId, {
    room_id: roomId,
    is_initiator: false
  });
}

export async function rejectRoom(io, roomId, myPortalId, partnerPortalId) {
  const room = await Room.findRoom(roomId);
  if (room === null || room === undefined) {
    // This room id NOT already exists
    return;
  }

  const myDevice = await Device.findDevice(myPortalId);
  if (myDevice === undefined || myDevice === null || myDevice.presenceStatus === PRESENCE_STATUS.offline) {
    return;
  }

  const partnerDevice = await Device.findDevice(partnerPortalId);
  if (partnerDevice === undefined || partnerDevice === null || myDevice.presenceStatus === PRESENCE_STATUS.offline) {
    return;
  }

  io.to(partnerDevice.socketId).emit("rejectedRoom", partnerDevice.portalId, {
    room_id: room.roomId,
    caller_portal_id: partnerDevice.portalId,
    callee_portal_id: myDevice.portalId
  });

  await RoomMember.removeRoomMembers(room.roomId);
  await Room.removeRoom(room.roomId);
}

export async function exitAllRoom(io, myPortalId) {
  const myDevice = await Device.findDevice(myPortalId);
  if (myDevice === undefined || myDevice === null || myDevice.presenceStatus === PRESENCE_STATUS.offline) {
    return;
  }
  const roomMembers = await RoomMember.findRoomMembers(myDevice);
  await RoomMember.removeRoomMembersByPortalId(myPortalId);

  roomMembers.forEach(async function (member, index) {
    const roomId = member.roomId;
    const stayDevices = await RoomMember.findDevices(roomId);
    if (stayDevices == null || stayDevices === undefined || stayDevices.length === 0) {
      Room.removeRoom(roomId);
    }
    stayDevices.forEach(function (device) {
      io.to(device.socketId)
        .emit("friendExitedRoom", device.portalId, {
          room_id: member.roomId,
          friend_portal_id: myPortalId
        });
    });
  });
}

export async function sendPhotoUpdateRoom(io, myPortalId, roomId, photo_base64) {
  const room = await Room.findRoom(roomId);
  if (room === null || room === undefined) {
    // This room id NOT already exists
    return;
  }

  const myDevice = await Device.findDevice(myPortalId);
  if (myDevice === undefined || myDevice === null || myDevice.presenceStatus === PRESENCE_STATUS.offline) {
    return;
  }

  const devices = await RoomMember.findDevices(room.roomId);
  devices.forEach(function (device) {
    if (device.portalId === myPortalId) {
      return;
    }
    io.to(device.socketId).emit("receivedPhotoUpdate", device.portalId, {
      room_id: room.roomId,
      peer_portal_id: myDevice.portalId,
      photo_base64: photo_base64
    });
  });


}

export async function sendPhotoModeChangedRoom(io, myPortalId, roomId, active) {
  const room = await Room.findRoom(roomId);
  if (room === null || room === undefined) {
    // This room id NOT already exists
    return;
  }

  const myDevice = await Device.findDevice(myPortalId);
  if (myDevice === undefined || myDevice === null || myDevice.presenceStatus === PRESENCE_STATUS.offline) {
    return;
  }

  const devices = await RoomMember.findDevices(room.roomId);
  devices.forEach(function (device) {
    if (device.portalId === myPortalId) {
      return;
    }
    io.to(device.socketId).emit("changedPhotoMode", device.portalId, {
      room_id: room.roomId,
      peer_portal_id: myDevice.portalId,
      active: active
    });
  });
}

export async function rejectContact(io, myPortalId, partnerPortalId) {
  const myDevice = await Device.findDevice(myPortalId);
  if (myDevice === undefined || myDevice === null || myDevice.presenceStatus === PRESENCE_STATUS.offline) {
    return;
  }

  const partnerDevice = await Device.findDevice(partnerPortalId);
  if (partnerDevice === undefined || partnerDevice === null || partnerDevice.presenceStatus === PRESENCE_STATUS.offline) {
    return;
  }

  io.to(partnerDevice.socketId).emit("rejectedContact", partnerDevice.portalId, {
    contact_portal_id: myDevice.portalId,
    contact_device_name: myDevice.deviceName
  });
}

export async function acceptContact(io, myPortalId, partnerPortalId) {
  const myDevice = await Device.findDevice(myPortalId);
  if (myDevice === undefined || myDevice === null || myDevice.presenceStatus === PRESENCE_STATUS.offline) {
    return;
  }

  const partnerDevice = await Device.findDevice(partnerPortalId);
  if (partnerDevice === undefined || partnerDevice === null || partnerDevice.presenceStatus === PRESENCE_STATUS.offline) {
    return;
  }

  io.to(partnerDevice.socketId).emit("acceptedContact", partnerDevice.portalId, {
    contact_portal_id: myDevice.portalId,
    contact_device_name: myDevice.deviceName
  });
}

export async function addContact(io, myPortalId, partnerPortalId) {
  const myDevice = await Device.findDevice(myPortalId);
  if (myDevice === undefined || myDevice === null || myDevice.presenceStatus === PRESENCE_STATUS.offline) {
    return;
  }

  const partnerDevice = await Device.findDevice(partnerPortalId);
  if (partnerDevice === undefined || partnerDevice === null || partnerDevice.presenceStatus === PRESENCE_STATUS.offline) {
    return;
  }

  io.to(partnerDevice.socketId).emit("requestedContact", partnerDevice.portalId, {
    contact_portal_id: myDevice.portalId,
    contact_device_name: myDevice.deviceName
  });
}