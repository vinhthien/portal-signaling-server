export enum NOTIFICATION_TYPE {
  AddContact = "AddContact",
  AcceptContact = "AcceptContact",
  InvalidDevice = "InvalidDevice",
}

export enum SCREEN_TYPE {
  ContactScreen = "contact_screen",
  LoginScreen = "login_screen",
}