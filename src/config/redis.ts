import redis from "redis";

export const connection = {
  host: process.env.REDIS_HOST || "127.0.0.1",
  port: Number(process.env.REDIS_PORT || 6379),
  ... typeof process.env.REDIS_PASSWORD !== "undefined" && {password: process.env.REDIS_PASSWORD},
};
export const client = redis.createClient(connection);