export enum ROOM_MEMBER_STATUS {
    joined = "joined",
    exited = "exited"
}

export enum ROOM_MEMBER_TYPE {
    host = "host",
    normal = "normal",
}