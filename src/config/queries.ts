export const getCount = () => {
  return [
    { $group: {
      _id: null,
      count: { $sum: 1 }
    } }
  ];
};