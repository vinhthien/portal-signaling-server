export const PAGINATION_LIMIT = 100;
export const PAGINATION_DEFAULT = 20;

export const SORT_DIR = {
  desc: "desc",
  asc: "asc",
};

export const FLASH_TYPE = {
  error: "error",
  info: "info",
  success: "success",
};

export const DATE_FORMAT = "YYYY/MMM/DD";
export const DATE_TIME_FORMAT = "YYYY/MMM/DD HH:mm:ss";
export const EXPORT_DATE_TIME_FORMAT = "YYYY-MM-DD HH:mm:ss";