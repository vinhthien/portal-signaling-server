import _padStart from "lodash/padStart";

export function lengthRegExpFactory(min = 0, max = 0, characterGroup = ".", flags = "") {
  return new RegExp(`^(${characterGroup}){${min},${max}}$`, flags);
}

export function makeRandomNumericString(length: number) {
  if (length <= 0) return "";
  return _padStart(Math.floor(Math.random() * (10 ** length)).toString(), length, "0");
}