import admin from "firebase-admin";
import { logFirebase } from "./log";
import _isString from "lodash/isString";

const FIREBASE_KEY_JSON = process.env.FIREBASE_KEY_JSON;
const serviceAccount = _isString(FIREBASE_KEY_JSON) ? JSON.parse(FIREBASE_KEY_JSON) : FIREBASE_KEY_JSON;
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});

export async function pushNotification(title, body, token) {
  try {
    const message = {
      notification: {
        title,
        body,
      },
      token,
    };

    const response = await admin.messaging().send(message);
    return response;
  } catch (error) {
    logFirebase(error);
    return null;
  }
}

export async function pushData(data, token) {
  try {
    let notification;
    if (data.title || data.body) {
      const { title, body } = data;
      notification = {
        title,
        body,
      };
    }

    let badge;
    if (data.badge) {
      badge = { badge: Number(data.badge) };
    }

    const message = {
      data,
      token,
      apns: {
        headers: {
          "apns-priority": "10",
        },
        payload: {
          aps: {
            ... typeof badge !== "undefined" && badge,
            sound: "default",
          },
        },
      },
      ... typeof notification !== "undefined" && { notification },
    };
    const response = await admin.messaging().send(message);
    return response;
  } catch (error) {
    logFirebase(error);
    return null;
  }
}