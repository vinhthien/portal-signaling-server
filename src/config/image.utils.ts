import path from "path";
import nodeUrl from "url";
import moment from "moment";
import { promisify } from "bluebird";
import sharp from "sharp";
import {
  readFile as readFileSync,
  writeFile as writeFileSync,
  mkdir as mkdirSync,
} from "fs";
import { Request } from "express";

const readFile = promisify(readFileSync);
const writeFile: any = promisify(writeFileSync);
const mkdir: any = promisify(mkdirSync);
const WEBSITE_URL = process.env.WEBSITE_URL;

// Decoding base-64 image
// Source: http://stackoverflow.com/questions/20267939/nodejs-write-base64-image-file
export interface DecodedBase64Image {
  type: string;
  data: string;
}
export function decodeBase64Image(dataString: string): DecodedBase64Image {
  const matches = dataString.match(/^data:([A-Za-z-+/]+);base64,(.+)$/);
  const response: any = {};

  if (matches.length !== 3) throw "Invalid input string";

  response.type = matches[1];
  response.data = Buffer.from(matches[2], "base64");

  return response;
}

export function encodeBase64Image(dataImage: (string | Buffer), fileType: string): string {
  const data = Buffer.from(dataImage).toString("base64");
  const response = `data:image/${fileType};base64,${data}`;

  return response;
}

// adapted from https://stackoverflow.com/a/40686853
export async function mkdirByPath(targetDir: string, baseDir = "."): Promise<void> {
  const sep = path.sep;
  const initDir = path.isAbsolute(targetDir) ? sep : "";

  // TODO: use fs.access to check full path exists first? could cut down on unnecessary calls if path is deep
  await targetDir.split(sep).reduce(async (parentDirP, childDir) => {
    const parentDir = await parentDirP;
    const curDir = path.resolve(baseDir, parentDir, childDir);
    try {
      await mkdir(curDir);
      console.log(`Directory ${curDir} created!`);
    } catch (err) {
      if (err.code !== "EEXIST") {
        throw err;
      }

      // console.log(`Directory ${curDir} already exists!`);
    }

    return curDir;
  }, Promise.resolve(initDir));
}

/*
  Save base64 image to disk, based on https://stackoverflow.com/a/26397588
  imagePath sould be relative to this filepath (i.e., /src/config/utils.js)
*/
export function getExtFromImgType(imageType: string): string {
  // Regular expression for image type:
  // This regular image extracts the "jpeg" from "image/jpeg"
  const imageTypeRE = /\/(.*?)$/;
  return imageType.match(imageTypeRE)[1];
}

export async function saveImageBuffer(imageBuffer: (string | Buffer), imageType: string, imagePath: string, options: any = {}): Promise<string> {
  try {
    const imageName = options.hasOwnProperty("imageName")
      ? options.imageName
      : `image-${moment().valueOf()}`;
    let fileName = `${imageName}.${imageType}`;
    if (imagePath.trim() === "") imagePath = ".";
    let fullPath = path.normalize(`${imagePath}/${fileName}`);
    let { maxWidth, maxHeight } = options;
    const { quality } = options;

    try {
      await mkdirByPath(imagePath, __dirname);

      if (typeof maxWidth !== "undefined" || typeof maxHeight !== "undefined") {
        // perform resizing/cropping
        const image = sharp(imageBuffer);
        let {width, height} = await image.metadata();

        // fill in missing parameter if only one max dimension is specified
        // i.e., resize, but force keeping aspect ratio

        if (typeof maxWidth === "undefined") {
          maxWidth = Math.floor(maxHeight / height * width);
        } else if (typeof maxHeight === "undefined") {
          maxHeight = Math.floor(maxWidth / width * height);
        }

        const ratio = maxWidth / maxHeight;
        const originalRatio = width / height;
        if (width > maxWidth || height > maxHeight || ratio !== originalRatio) {
          if (ratio > 1) {
            // width > height
            height = Math.min(maxHeight, height);
            width = Math.floor(height * ratio);
          } else {
            // width <= height
            width = Math.min(maxWidth, width);
            height = Math.floor(width / ratio);
          }

          if (typeof quality !== "undefined") {
            // force to jpeg as well
            fileName = `${imageName}.jpeg`;
            fullPath = path.normalize(`${imagePath}/${fileName}`);
            await image
              .resize(width, height)
              .jpeg({
                quality,
              })
              .toFile(path.resolve(__dirname, fullPath));
          } else {
            await image.resize(width, height)
              .toFile(path.resolve(__dirname, fullPath));
          }
        } else {
          // no resizing/cropping needed
          if (typeof quality !== "undefined") {
            // force to jpeg as well
            fileName = `${imageName}.jpeg`;
            fullPath = path.normalize(`${imagePath}/${fileName}`);
            await image
              .jpeg({
                quality: 70,
              })
              .toFile(path.resolve(__dirname, fullPath));
          } else {
            await writeFile(path.resolve(__dirname, fullPath), imageBuffer);
          }
        }
      } else {
        // don"t perform resizing/cropping, i.e., original method
        await writeFile(path.resolve(__dirname, fullPath), imageBuffer);
      }

      const url = path.normalize(
        path.relative(".", path.resolve(__dirname, imagePath)) +
        `/${fileName}`
      );
      return url;
    } catch (err) {
      console.log(err);
      throw 500;
    }
  } catch (err) {
    console.log(err);
    throw 400;
  }
}

export async function saveBase64Image(imageData: string, imagePath: string, options: any = {}): Promise<string> {
  try {
    const { data, type } = decodeBase64Image(imageData);
    // This variable is actually an array which has 5 values, The [1] value is the real image extension
    const imageType = getExtFromImgType(type);
    const url = await saveImageBuffer(data, imageType, imagePath, options);
    return url;
  } catch (err) {
    console.log(err);
    throw 400;
  }
}

/*
  imagePath sould be relative to this filepath (i.e., /src/config/utils.js)
*/
export async function loadBase64Image(imagePath: string) {
  try {
    const i = imagePath.lastIndexOf(".");
    if (i < 0) throw "Could not find file extension";

    const imageType = imagePath.substr(i + 1);
    const image = await readFile(path.resolve(__dirname, imagePath));
    const imageData = encodeBase64Image(image, imageType);

    return imageData;
  } catch (err) {
    console.log(err);
    throw 500;
  }
}

export function getImageUrl(req: Request, imageUrl: string): string {
  return imageUrl ? nodeUrl.format({
    protocol: req.protocol,
    host: WEBSITE_URL || req.get("host"),
    pathname: imageUrl,
  }) : "";
}