import path from "path";

export enum MEMBERSHIP_STATUSES {
  free = "free",
  basic = "basic",
  premium = "premium",
}

export enum ACCOUNT_STATUSES {
  active = "active",
  suspended = "suspended",
}

export const PATH_TO_AVATAR = path.resolve(__dirname, "../public/avatar/");
export const AVATAR_DIMS = {
  width: 1,
  height: 1,
};