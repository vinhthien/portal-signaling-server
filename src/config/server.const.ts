export const SALT_WORK_FACTOR = 10;
export const BODY_PARSER_LIMIT = "20mb";
export const CACHE_MAX_AGE = 86400000; // 1 day (in ms)