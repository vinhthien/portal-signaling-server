import { PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH } from "./auth.const";

export const EMAIL_RE = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; // taken from http://emailregex.com/
export const PW_RE = new RegExp(`^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]{${PASSWORD_MIN_LENGTH},${PASSWORD_MAX_LENGTH}})$`);