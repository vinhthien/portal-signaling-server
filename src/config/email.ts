import nodemailer from "nodemailer";

const MAIL_SECURE = typeof process.env.MAIL_SECURE !== "undefined" && process.env.MAIL_SECURE.toLowerCase() === "true" ? true : false;
const MAIL_PORT = parseInt(process.env.MAIL_PORT);

const auth = {
  ... typeof process.env.MAIL_USER !== "undefined" && {user: process.env.MAIL_USER},
  ... typeof process.env.MAIL_PASS !== "undefined" && {pass: process.env.MAIL_PASS},
};

const config = {
  host: process.env.MAIL_URL || "",
  port: MAIL_PORT || 25,
  secure: MAIL_SECURE,
  ignoreTLS: !MAIL_SECURE,
  auth,
  connectionTimeout: 10000,
};

export const transport = nodemailer.createTransport(config);

export function sendText(from, to, subject, text) {
  return transport.sendMail({ from, subject, to, text });
}

export function sendHtml(from, to, subject, html) {
  return transport.sendMail({ from, subject, to, html });
}