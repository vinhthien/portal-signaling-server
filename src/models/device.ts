import mongoose = require("mongoose");
import { DeleteWriteOpResultObject } from "mongodb";

import { IDeviceModel, IDeviceSchema } from "./device.interface";
import { makeRandomNumericString } from "../config/utils";
import User from "./user";
import { DEVICE_TYPES, PRESENCE_STATUS } from "../config/device.const";

const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

const DeviceSchema = <IDeviceSchema> new Schema({
  portalId: {
    type: String,
    required: true,
    unique: true,
  },
  deviceId: {
    type: String,
    required: true,
  },
  userId: {
    type: ObjectId,
    required: true,
  },
  deviceName: {
    type: String,
    required: true,
  },
  deviceType: {
    type: String,
    enum: Object.values(DEVICE_TYPES),
    required: true,
  },
  username: {
    type: String,
    required: true,
  },
  presenceStatus: {
    type: String,
    enum: Object.values(PRESENCE_STATUS),
    required: true,
  },
  pushToken: {
    type: String,
    required: false,
  },
  socketId: {
    type: String,
    required: false,
  },
}, {
  timestamps: true
});

DeviceSchema.index({ userId: 1, deviceId: 1 }, { unique: true });
DeviceSchema.index({ userId: 1, deviceName: 1 }, { unique: true });

DeviceSchema.statics.makePortalId = async function () {
  let portalId = undefined;
  let device = undefined;

  do {
    portalId = `${makeRandomNumericString(3)}-${makeRandomNumericString(3)}-${makeRandomNumericString(3)}`;
    device = await this.findOne({portalId});
  } while (device);

  return portalId;
};

DeviceSchema.statics.findDevice = async function (portalId) {
  return this.findOne({ portalId });
};

DeviceSchema.statics.removeDevice = function (userId, deviceId) {
  return this.deleteOne({ userId, deviceId })
  .then((result: DeleteWriteOpResultObject["result"]) => {
    const { ok, n } = result;

    return ok && n;
  });
};

DeviceSchema.statics.removeDeviceByPortalId = function(portalId) {
  return this.deleteOne({ portalId })
    .then((result: DeleteWriteOpResultObject["result"]) => {
      const { ok, n } = result;

      return ok && n;
    });
};

DeviceSchema.statics.renameDevice = function (userId, portalId, deviceName) {
  return this.findOneAndUpdate(
    { userId, portalId },
    { $set: { deviceName } },
    { new: true }
  );
};

DeviceSchema.statics.updatePresenceStatus = function (portalId, presenceStatus, socketId) {
  return this.findOneAndUpdate(
    { portalId },
    { $set: { presenceStatus, socketId } },
    { new: true }
  );
};

DeviceSchema.statics.updatePushToken = function (userId, deviceId, pushToken) {
  return this.findOneAndUpdate(
    { userId, deviceId },
    { $set: { pushToken } },
    { new: true }
  );
};

DeviceSchema.statics.removePushToken = function (userId, deviceId) {
  return this.findOneAndUpdate(
    { userId, deviceId },
    { $unset: { pushToken: 1 } },
    { new: true }
  );
};

DeviceSchema.statics.getDeviceByDeviceId = function (userId, deviceId) {
  return this.findOne({ userId, deviceId });
};

DeviceSchema.statics.getDeviceByDeviceName = function (userId, deviceName) {
  return this.findOne({ userId, deviceName });
};

DeviceSchema.statics.getDevices = function(userId) {
  return this.find({ userId }).then(devices => devices.map(device => device.getDeviceInfo()));
};

DeviceSchema.statics.associateDevice = async function (userId, deviceId, deviceName, deviceType) {
  // check for deviceName for userId
  // and reassociate existing portalId if name exists
  const device = await this.findOneAndUpdate(
    { userId, deviceName },
    { $set: { deviceId, deviceType } },
    { new: true }
  );
  if (device) return device;

  // (if deviceName does not exist) create portalId
  const portalId = await DeviceSchema.statics.makePortalId.bind(this)();

  const user = await User.findOne({_id: userId});
  const { username } = user;
  const presenceStatus = PRESENCE_STATUS.online;

  const newDevice = new this({
    userId,
    deviceId,
    deviceName,
    portalId,
    username,
    deviceType,
    presenceStatus,
  });

  return newDevice.save();
};

DeviceSchema.statics.updateDevicesForUser = function(userId, options) {
  return this.updateMany({ userId }, { $set: options });
};

DeviceSchema.methods.updatePushToken = function (pushToken) {
  const device = this;
  return device.update(
    { $set: { pushToken } }
  ).exec();
};

DeviceSchema.methods.getDeviceInfo = function () {
  const device = this;

  const {
    deviceId,
    deviceName,
    portalId,
    deviceType,
  } = device;

  const info = {
    deviceId,
    deviceName,
    portalId,
    deviceType,
  };

  return info;
};

DeviceSchema.methods.getPublicDeviceInfo = function () {
  const device = this;

  const {
    deviceName,
    portalId,
    username,
  } = device;

  const info = {
    deviceName,
    portalId,
    username,
  };

  return info;
};

const Device = <IDeviceModel> mongoose.model("Device", DeviceSchema);
export default Device;