import { Document, Model, Schema } from "mongoose";
import { Request } from "express";
import { IAuth, IAuthProps, IAuthMethods } from "./auth.interface";
import { ACCOUNT_STATUSES, MEMBERSHIP_STATUSES } from "../config/user.const";

export interface IUserProps extends IAuthProps {
  email: string;
  password: string;
  username: string;
  verified: boolean;
  accountStatus: ACCOUNT_STATUSES;
  membershipStatus: MEMBERSHIP_STATUSES;
  joinedTimestamp: Date;
  CreatedAt: Date;
  UpdatedAt: Date;
  verificationCode?: string;
  verificationCodeTimestamp?: Date;
  avatarUrl?: string;

  deviceId?: string; // injected from jwt
}

export interface AccountSummary {
  email: string;
  username: string;
  avatarUrl: string;
}

export interface IUserMethods extends IAuthMethods {
  getAccountSummary(req: Request): AccountSummary;
  getAvatarImage(req: Request): string;
  makeVerificationCode(): Promise<string>;
  updateAvatar(avatarData: string): Promise<IUser>;
  updateUsername(username: string): Promise<IUser>;
}

export interface IUserStatics {
  registerUser(email: string, password: string, username: string): Promise<IUser>;
  verifyUser(email: string, verificationCode: string): Promise<IUser>;
  checkVerificationCode(email: string, verificationCode: string): Promise<IUser>;
}

export interface IUserSchema extends Schema, IUserProps {
  statics: IUserStatics;
  methods: IUserMethods;
}

export interface IUser extends Document, IAuth, IUserProps, IUserMethods {}

export interface IUserModel extends Model<IUser>, IUserStatics {}