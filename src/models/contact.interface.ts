import { Document, Schema, Model } from "mongoose";
import { IDevice } from "./device.interface";

export enum ContactStatus {
  pending = "pending",
  requested = "requested",
  accepted = "accepted",
  blocked = "blocked",
}

export interface IContactProps {
  userId: Schema.Types.ObjectId;
  targetId: Schema.Types.ObjectId;
  userPortalId: string;
  targetPortalId: string;
  status: ContactStatus;
  autoAccept: boolean;
}

export interface ContactDetails {
  portalId: string;
  deviceName: string;
  autoAccept: boolean;
  username: string;
}

export interface RequestDetails {
  portalId: string;
  deviceName: string;
  username: string;
}

export interface BlockedDetails {
  portalId: string;
  deviceName: string;
  username: string;
}

export interface IContactStatics {
  addContact(user: IDevice, target: IDevice, block?: boolean): Promise<void>;
  acceptContact(userPortalId: string, targetPortalId: string): Promise<void>;
  blockContact(userPortalId: string, targetPortalId: string): Promise<IContact>;
  rejectContact(userPortalId: string, targetPortalId: string): Promise<void>;
  removeDeviceContacts(userPortalId: string): Promise<void>;
  removeContact(userPortalId: string, targetPortalId: string): Promise<void>;
  getContact(userPortalId: string, targetPortalId: string): Promise<IContact>;
  getContacts(userPortalId: string): Promise<ContactDetails[]>;
  getContactRequests(userPortalId: string): Promise<RequestDetails[]>;
  getBlockedContacts(userPortalId: string): Promise<BlockedDetails[]>;
  setAutoAccept(userPortalId: string, targetPortalId: string, autoAccept: boolean): Promise<IContact>;
}

export interface IContact extends IContactProps, Document {}

export interface IContactSchema extends IContactProps, Schema {
  statics: IContactStatics;
}

export interface IContactModel extends Model<IContact>, IContactStatics {}