import mongoose from "mongoose";
import bcrypt from "bcrypt";
import path from "path";
import moment from "moment";
import _isEmpty from "lodash/isEmpty";
import _trim from "lodash/trim";
import _values from "lodash/values";
import { makeRandomNumericString } from "../config/utils";

import { IUser, IUserModel, IUserSchema } from "./user.interface";

import { SALT_WORK_FACTOR } from "../config/server.const";
import {
  EMAIL_RE, PW_RE,
} from "../config/auth.regex";
import { saveBase64Image, getImageUrl } from "../config/image.utils";
import { MAX_IMAGE_WIDTH, JPEG_QUALITY } from "../config/image.const";
import {
  AVATAR_DIMS, PATH_TO_AVATAR, MEMBERSHIP_STATUSES, ACCOUNT_STATUSES,
} from "../config/user.const";

// can use __dirname as a cheat since this script is at the same depth as src/config/utils.js
const avatarSaveLocation = path.relative(__dirname, PATH_TO_AVATAR);

const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

const UserSchema = <IUserSchema> new Schema({
  email: {
    type: String,
    required: true,
    unique: true,
    validate: {
      validator: function(v) {
        return EMAIL_RE.test(v);
      },
      message: "{VALUE} is not a valid email address!",
    },
  },
  password: {
    type: String,
    required: true,
  },
  verified: {
    type: Boolean,
    required: true,
    default: false,
  },
  accountStatus: {
    type: String,
    required: true,
    default: ACCOUNT_STATUSES.active,
    enum: _values(ACCOUNT_STATUSES),
  },
  membershipStatus: {
    type: String,
    required: true,
    default: MEMBERSHIP_STATUSES.free,
    enum: _values(MEMBERSHIP_STATUSES),
  },
  verificationCode: {
    type: String,
    required: false,
  },
  verificationCodeTimestamp: {
    type: Date,
    required: false,
  },
  username: {
    type: String,
    required: true,
    validate: {
      validator: function(v) {
        return !_isEmpty(_trim(v));
      },
      message: "{VALUE} is not a valid username!",
    },
  },
  avatarUrl: {
    type: String,
    required: false,
  },
  joinedTimestamp: {
    type: Date,
    required: true,
    default: Date.now,
  },
}, {
  timestamps: true
});

UserSchema.pre("save", function(next) {
  const user = <IUser> this;

  // only hash the password if it has been modified (or is new)
  if (!user.isModified("password")) return next();
  if (!PW_RE.test(user.password)) return next(Error("Invalid password."));

  // generate a salt
  bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
    if (err) return next(err);

    // hash the password along with our new salt
    bcrypt.hash(user.password, salt, function(err, hash) {
      if (err) return next(err);

      // override the cleartext password with the hashed one
      user.password = hash;
      return next();
    });
  });
});

function updateHook(next) {
  const user = this;
  const passwordModified = user._update.$set.password || user._update.password;

  // only hash the password if it has been modified
  if (passwordModified) {
    const password = user._update.$set.password || user._update.password;
    if (!PW_RE.test(password)) return next(Error("Invalid password."));

    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
      if (err) return next(err);

      // hash the password along with our new salt
      bcrypt.hash(password, salt, function(err, hash) {
        if (err) return next(err);

        // override the cleartext password with the hashed one
        if (user._update.$set.password) user._update.$set.password = hash;
        else if (user._update.password) user._update.password = hash;
        return next();
      });
    });
  }

  if (!passwordModified) return next();
}
UserSchema.pre("update", updateHook);
UserSchema.pre("findOneAndUpdate", updateHook);

UserSchema.methods.comparePassword = function(candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
    if (err) return cb(err.message);
    cb(null, isMatch);
  });
};

UserSchema.methods.changePassword = function(password) {
  const user = this;
  user.password = password;
  return user.save();
};

UserSchema.methods.makeVerificationCode = async function () {
  const user = this;

  const verificationCode = makeRandomNumericString(4);
  await user.update({ $set: {
    verificationCode,
    verificationCodeTimestamp: new Date(),
  } });

  return verificationCode;
};

UserSchema.methods.updateAvatar = async function(avatarData) {
  const user = this;
  const { id } = user;

  /*** process avatar ***/
  const year = moment().format("YYYY");
  const month = moment().format("MM");

  const image_ts = moment().valueOf();
  const avatarDynamicPath = `${year}/${month}`;
  const avatarFullPath = path.normalize(`${avatarSaveLocation}/${avatarDynamicPath}`);
  const imageName = `avatar-${id}-${image_ts}`;
  const avatarOptions = {
    imageName,
    maxWidth: MAX_IMAGE_WIDTH,
    maxHeight: Math.floor(MAX_IMAGE_WIDTH / AVATAR_DIMS.width * AVATAR_DIMS.height),
    quality: JPEG_QUALITY,
  };

  let avatarUrl;
  avatarUrl = await saveBase64Image(avatarData, avatarFullPath, avatarOptions);
  // TODO: better way to create correct path?
  avatarUrl = path.relative(path.resolve(__dirname, "../../dist/public"), avatarUrl);
  /*** finish processing avatar ***/

  user.avatarUrl = avatarUrl;
  return user.save();
};

UserSchema.methods.updateUsername = async function(username) {
  const user = this;
  user.username = username;
  return user.save();
};

UserSchema.methods.getAccountSummary = function(req) {
  const user = this;

  const {
    email,
    username,
  } = user;
  const avatarUrl = user.getAvatarImage(req);

  const account = {
    email,
    username,
    avatarUrl,
  };

  return account;
};

UserSchema.methods.getAvatarImage = function(req) {
  const user = this;
  let { avatarUrl } = user;
  avatarUrl = getImageUrl(req, avatarUrl);
  return avatarUrl;
};

UserSchema.statics.registerUser = async function(email, password, username) {
  // create user!
  let user;

  try {
    user = new this({
      email,
      password,
      username,
      joinedTimestamp: new Date(),
    });
    await user.save();
  } catch (err) {
    console.log(err);
    if (typeof err !== "undefined" && err.code === 11000) {
      // user already exists; try updating user
      user = await this.findOneAndUpdate({
        email,
        verified: false,
      }, {
        $set: {
          password,
          username,
          joinedTimestamp: new Date(),
        },
      }, { new: true });
    }
  }

  return user;
};

UserSchema.statics.verifyUser = function(email, verificationCode) {
  return this.findOneAndUpdate({
    email,
    verificationCode,
  }, {
    $set: { verified: true },
    $unset: {
      verificationCode: 1,
      verificationCodeTimestamp: 1,
    },
  }, { new: true });
};

UserSchema.statics.checkVerificationCode = function(email, verificationCode) {
  return this.findOne({ email, verificationCode });
};

const User = <IUserModel> mongoose.model("User", UserSchema);
export default User;