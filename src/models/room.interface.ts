import { Document, Model, Schema } from "mongoose";
import { BulkWriteOpResultObject } from "mongodb";
import { IDevice } from "./device.interface";
import { IRoomMember } from "./room-member.interface";

export interface IRoomProps {
  roomId: string;
}

export interface IRoomMethods {
}

export interface IRoomStatics {
  findRoom: (roomId: string) => Promise<IRoom>;
  removeRoom: (roomId: string) => Promise<boolean>;
}

export interface IRoomSchema extends Schema, IRoomProps {
  statics: IRoomStatics;
  methods: IRoomMethods;
}

export interface IRoom extends Document, IRoomProps, IRoomMethods {}

export interface IRoomModel extends Model<IRoom>, IRoomStatics {}