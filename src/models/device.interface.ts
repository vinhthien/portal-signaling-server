import { Document, Model, Schema } from "mongoose";
import { BulkWriteOpResultObject } from "mongodb";

export interface IDeviceProps {
  portalId: string;
  deviceId: string;
  userId: Schema.Types.ObjectId;
  deviceName: string;
  deviceType: string;
  username: string;
  presenceStatus: string;
  pushToken: string;
  socketId: string;
}

export interface DeviceInfo {
  portalId: string;
  deviceId: string;
  deviceName: string;
  deviceType: string;
}

export interface PublicDeviceInfo {
  portalId: string;
  deviceName: string;
  username: string;
}

export interface DevicesUpdate {
  username?: string;
}

export interface IDeviceMethods {
  getDeviceInfo: () => DeviceInfo;
  getPublicDeviceInfo: () => PublicDeviceInfo;
  updatePushToken: (pushToken: String) => Promise<void>;
}

export interface IDeviceStatics {
  associateDevice: (userId: Schema.Types.ObjectId, deviceId: string, deviceName: string, deviceType: string) => Promise<IDevice>;
  findDevice: (portalId: string) => Promise<IDevice>;
  removeDevice: (userId: Schema.Types.ObjectId, deviceId: string) => Promise<boolean>;
  removeDeviceByPortalId: (portalId: string) => Promise<boolean>;
  renameDevice: (userId: Schema.Types.ObjectId, deviceId: string, deviceName: string) => Promise<IDevice>;
  getDeviceByDeviceId: (userId: Schema.Types.ObjectId, deviceId: string) => Promise<IDevice>;
  getDeviceByDeviceName: (userId: Schema.Types.ObjectId, deviceName: string) => Promise<IDevice>;
  getDevices: (userId: Schema.Types.ObjectId) => Promise<DeviceInfo[]>;
  makePortalId: () => Promise<string>;
  updateDevicesForUser: (userId: Schema.Types.ObjectId, updates: DevicesUpdate) => Promise<BulkWriteOpResultObject>;
  updatePresenceStatus: (portalId: String, presenceStatus: String, socketId: String) => Promise<IDevice>;
  updatePushToken: (userId: Schema.Types.ObjectId, deviceId: string, pushToken: String) => Promise<IDevice>;
  removePushToken: (userId: Schema.Types.ObjectId, deviceId: string) => Promise<IDevice>;
}

export interface IDeviceSchema extends Schema, IDeviceProps {
  statics: IDeviceStatics;
  methods: IDeviceMethods;
}

export interface IDevice extends Document, IDeviceProps, IDeviceMethods {}

export interface IDeviceModel extends Model<IDevice>, IDeviceStatics {}