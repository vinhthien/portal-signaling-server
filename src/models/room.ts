import mongoose = require("mongoose");
import { DeleteWriteOpResultObject } from "mongodb";

import { IRoomModel, IRoomSchema } from "./room.interface";
import { makeRandomNumericString } from "../config/utils";

const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

const RoomSchema = <IRoomSchema> new Schema({
    roomId: {
    type: String,
    required: true,
    unique: true,
  },
}, {
  timestamps: true
});

RoomSchema.statics.findRoom = async function (roomId) {
    return this.findOne({ roomId });
};

RoomSchema.statics.removeRoom = async function (roomId) {
  return this.deleteOne({ roomId })
    .then((result: DeleteWriteOpResultObject["result"]) => {
      const { ok, n } = result;

      return ok && n;
    });
};

const Room = <IRoomModel> mongoose.model("Room", RoomSchema);
export default Room;