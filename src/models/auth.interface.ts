export interface IAuthProps {
  password: string;
}

export interface IAuthMethods {
  comparePassword(candidatePassword: string, cb: (error: string, isMatch?: boolean) => void): void;
  changePassword<T>(password: string): Promise<T>;
}

export interface IAuth extends IAuthProps, IAuthMethods {}