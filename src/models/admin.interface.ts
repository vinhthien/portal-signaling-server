import { Document, Schema, Model } from "mongoose";
import { IAuth, IAuthProps, IAuthMethods } from "./auth.interface";

export interface IAdminProps extends IAuthProps {
  email: string;
  password: string;
  username: string;
}

export interface IAdminMethods extends IAuthMethods {}

export interface IAdminStatics {}

export interface IAdminSchema extends Schema, IAdminProps {
  statics: IAdminStatics;
  methods: IAdminMethods;
}

export interface IAdmin extends Document, IAuth, IAdminProps, IAdminMethods {}

export interface IAdminModel extends Model<IAdmin>, IAdminStatics {}