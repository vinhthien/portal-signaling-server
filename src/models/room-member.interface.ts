import { Document, Model, Schema } from "mongoose";
import { BulkWriteOpResultObject } from "mongodb";
import { IDevice } from "./device.interface";


export interface IRoomMemberProps {
  roomId: string;
  portalId: string;
  status: string;
  memberType: string;
}

export interface IRoomMemberMethods {
}

export interface IRoomMemberStatics {
  findRoomMember: (roomId: string, device: IDevice) => Promise<IRoomMember>;
  findRoomMembers: (device: IDevice) => Promise<IRoomMember[]>;
  updateStatus: (device: IDevice) => Promise<IRoomMember>;
  makeRoomMembers: (roomId: string, myDevice: IDevice, partnerDevice: IDevice) => Promise<boolean>;
  createRoomMember: (roomId: string, myDevice: IDevice, status: string) => Promise<IRoomMember>;
  findDevices: (roomId: string) => Promise<IDevice[]>;
  removeRoomMembers: (roomId: string) => Promise<boolean>;
  removeRoomMembersByPortalId: (portalId: string) => Promise<IRoomMember[]>;
}

export interface IRoomMemberSchema extends Schema, IRoomMemberProps {
  statics: IRoomMemberStatics;
  methods: IRoomMemberMethods;
}

export interface IRoomMember extends Document, IRoomMemberProps, IRoomMemberMethods {}

export interface IRoomMemberModel extends Model<IRoomMember>, IRoomMemberStatics {}