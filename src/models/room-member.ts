import mongoose = require("mongoose");
import { DeleteWriteOpResultObject } from "mongodb";

import { IRoomMemberModel, IRoomMemberSchema } from "./room-member.interface";
import Device from "./device";
import { ROOM_MEMBER_STATUS, ROOM_MEMBER_TYPE } from "../config/room-member.const";

const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

const RoomMemberSchema = <IRoomMemberSchema> new Schema({
  roomId: {
    type: String,
    required: true,
  },
  portalId: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    enum: ROOM_MEMBER_STATUS,
    required: true,
  },
  memberType: {
    type: String,
    enum: ROOM_MEMBER_TYPE,
    required: true,
  },
}, {
  timestamps: true
});

RoomMemberSchema.index({ roomId: 1, deviceId: 1 }, { unique: true });

RoomMemberSchema.statics.findRoomMember = async function (roomId, device) {
  const deviceId = device.deviceId;
  return this.findOne({ roomId, deviceId });
};

RoomMemberSchema.statics.findRoomMembers = async function (device) {
    const deviceId = device.deviceId;
    return this.find({ deviceId });
};

RoomMemberSchema.statics.makeRoomMembers = async function (roomId, myDevice, partnerDevice) {
  const myDeviceId = myDevice.deviceId;
  const myStatus = ROOM_MEMBER_STATUS.joined;
  const myType = ROOM_MEMBER_TYPE.host;
  const myRoomMember = new this({
    roomId,
    myDeviceId,
    myStatus,
    myType,
  });
  myRoomMember.save();

  const partnerDeviceId = myDevice.deviceId;
  const partnerStatus = ROOM_MEMBER_STATUS.joined;
  const partnerType = ROOM_MEMBER_TYPE.normal;
  const partnerRoomMember = new this({
      roomId,
      partnerDeviceId,
      partnerStatus,
      partnerType,
  });
  partnerRoomMember.save();

  return true;
};

RoomMemberSchema.statics.createRoomMember = async function (roomId, device, status) {
  const deviceId = device.deviceId;
  const type = ROOM_MEMBER_TYPE.normal;
  const roomMember = new this({
    roomId,
    deviceId,
    status,
    type,
  });
  return roomMember.save();
};

RoomMemberSchema.statics.findDevices = function(roomId) {
  return this.find({ roomId });
};

RoomMemberSchema.statics.removeRoomMembers = function(roomId) {
  return this.delete({ roomId })
    .then((result: DeleteWriteOpResultObject["result"]) => {
      const { ok, n } = result;

      return ok && n;
    });
};

RoomMemberSchema.statics.removeRoomMembersByPortalId = async function(portalId) {
  const device = await Device.findDevice(portalId);
  const deviceId = device._id;

  return this.delete({ deviceId })
    .then((result: DeleteWriteOpResultObject["result"]) => {
      const { ok, n } = result;

      return ok && n;
    });
};

const RoomMember = <IRoomMemberModel> mongoose.model("RoomMember", RoomMemberSchema);
export default RoomMember;