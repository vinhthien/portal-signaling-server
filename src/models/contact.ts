import mongoose from "mongoose";
import { ContactStatus, IContactSchema, IContactModel } from "./contact.interface";
import Device from "./device";

const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

const ContactSchema = <IContactSchema> new Schema({
  userId: {
    type: ObjectId,
    required: true,
  },
  targetId: {
    type: ObjectId,
    required: true,
  },
  userPortalId: {
    type: String,
    required: true,
  },
  targetPortalId: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    required: true,
  },
  autoAccept: {
    type: Boolean,
    required: true,
    default: false,
  },
});

ContactSchema.index({userPortalId: 1, targetPortalId: 1}, {unique: true});

ContactSchema.statics.addContact = async function(user, target, block = false) {
  const userData = {
    userId: user.userId,
    targetId: target.userId,
    userPortalId: user.portalId,
    targetPortalId: target.portalId,
    status: block ? ContactStatus.blocked : ContactStatus.pending,
  };
  const userContact = new this(userData);
  await userContact.save();

  if (!block) {
    const targetData = {
      userId: target.userId,
      targetId: user.userId,
      userPortalId: target.portalId,
      targetPortalId: user.portalId,
      status: ContactStatus.requested,
    };
    const targetContact = new this(targetData);
    await targetContact.save();
  }
};

ContactSchema.statics.blockContact = async function(userPortalId, targetPortalId) {
  // delete target's contact if available
  await this.deleteOne({
    userPortalId: targetPortalId,
    targetPortalId: userPortalId,
    status: { $ne: ContactStatus.blocked },
  });

  return this.findOneAndUpdate({
    userPortalId,
    targetPortalId,
  }, {
    $set: { status: ContactStatus.blocked }
  });
};

ContactSchema.statics.acceptContact = async function(userPortalId, targetPortalId) {
  await this.findOneAndUpdate({
    userPortalId,
    targetPortalId,
    status: ContactStatus.requested,
  }, {
    $set: { status: ContactStatus.accepted }
  });

  await this.findOneAndUpdate({
    userPortalId: targetPortalId,
    targetPortalId: userPortalId,
    status: ContactStatus.pending,
  }, {
    $set: { status: ContactStatus.accepted }
  });
};

ContactSchema.statics.rejectContact = async function(userPortalId, targetPortalId) {
  await this.deleteOne({
    userPortalId,
    targetPortalId,
    status: ContactStatus.requested,
  });

  await this.deleteOne({
    userPortalId: targetPortalId,
    targetPortalId: userPortalId,
    status: ContactStatus.pending,
  });
};

ContactSchema.statics.removeDeviceContacts = async function(userPortalId) {
  await this.deleteMany({
    $or: [
      { userPortalId },
      { targetPortalId: userPortalId },
    ]
  });
};

ContactSchema.statics.removeContact = async function(userPortalId, targetPortalId) {
  await this.deleteOne({
    userPortalId,
    targetPortalId,
  });

  await this.deleteOne({
    userPortalId: targetPortalId,
    targetPortalId: userPortalId,
    status: { $ne: ContactStatus.blocked },
  });
};

ContactSchema.statics.getContact = function(userPortalId, targetPortalId) {
  return this.findOne({
    userPortalId,
    targetPortalId,
  });
};

ContactSchema.statics.getContacts = function(userPortalId) {
  return this.aggregate([
    { $match: {
      userPortalId,
      status: ContactStatus.accepted,
    } },
    { $lookup: {
      from: Device.collection.name,
      localField: "targetPortalId",
      foreignField: "portalId",
      as: "device",
    } },
    {
      $replaceRoot: { newRoot: { $mergeObjects: [ { $arrayElemAt: [ "$device", 0 ] }, "$$ROOT" ] } }
    },
    { $project: {
      _id: 0,
      portalId: "$targetPortalId",
      autoAccept: 1,
      deviceName: 1,
      username: 1,
    } },
  ]);
};

ContactSchema.statics.getContactRequests = function(userPortalId) {
  return this.aggregate([
    { $match: {
      userPortalId,
      status: ContactStatus.requested,
    } },
    { $lookup: {
      from: Device.collection.name,
      localField: "targetPortalId",
      foreignField: "portalId",
      as: "device",
    } },
    {
      $replaceRoot: { newRoot: { $mergeObjects: [ { $arrayElemAt: [ "$device", 0 ] }, "$$ROOT" ] } }
    },
    { $project: {
      _id: 0,
      portalId: "$targetPortalId",
      deviceName: 1,
      username: 1,
    } },
  ]);
};

ContactSchema.statics.getBlockedContacts = function(userPortalId) {
  return this.aggregate([
    { $match: {
      userPortalId,
      status: ContactStatus.blocked,
    } },
    { $lookup: {
      from: Device.collection.name,
      localField: "targetPortalId",
      foreignField: "portalId",
      as: "device",
    } },
    {
      $replaceRoot: { newRoot: { $mergeObjects: [ { $arrayElemAt: [ "$device", 0 ] }, "$$ROOT" ] } }
    },
    { $project: {
      _id: 0,
      portalId: "$targetPortalId",
      deviceName: 1,
      username: 1,
    } },
  ]);
};

ContactSchema.statics.setAutoAccept = function(userPortalId, targetPortalId, autoAccept) {
  return this.findOneAndUpdate({
    userPortalId,
    targetPortalId,
    status: ContactStatus.accepted,
  }, { $set: {
    autoAccept,
  } });
};

const Contact = <IContactModel> mongoose.model("Contact", ContactSchema);
export default Contact;