import mongoose = require("mongoose");
import bcrypt = require("bcrypt");

import { IAdmin, IAdminSchema, IAdminModel } from "./admin.interface";

import { SALT_WORK_FACTOR } from "../config/server.const";
import {
  EMAIL_RE, PW_RE,
} from "../config/auth.regex";

const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

const AdminSchema = <IAdminSchema> new Schema({
  email: {
    type: String,
    required: true,
    unique: true,
    sparse: true,
    validate: {
      validator: function(v) {
        return EMAIL_RE.test(v);
      },
      message: "{VALUE} is not a valid email address!",
    },
  },
  password: {
    type: String,
    required: true,
  },
  username: {
    type: String,
    required: true,
  },
}, {
  timestamps: true
});

AdminSchema.pre("save", function(next) {
  const admin = <IAdmin> this;

  // only hash the password if it has been modified (or is new)
  if (!admin.isModified("password")) return next();
  if (!PW_RE.test(admin.password)) return next(Error("Invalid password."));

  // generate a salt
  bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
    if (err) return next(err);

    // hash the password along with our new salt
    bcrypt.hash(admin.password, salt, function(err, hash) {
      if (err) return next(err);

      // override the cleartext password with the hashed one
      admin.password = hash;
      return next();
    });
  });
});

function updateHook(next) {
  const admin = this;
  const passwordModified = admin._update.$set.password || admin._update.password;

  // only hash the password if it has been modified
  if (passwordModified) {
    const password = admin._update.$set.password || admin._update.password;
    if (!PW_RE.test(password)) return next(Error("Invalid password."));

    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
      if (err) return next(err);

      // hash the password along with our new salt
      bcrypt.hash(password, salt, function(err, hash) {
        if (err) return next(err);

        // override the cleartext password with the hashed one
        if (admin._update.$set.password) admin._update.$set.password = hash;
        else if (admin._update.password) admin._update.password = hash;
        return next();
      });
    });
  }

  if (!passwordModified) return next();
}
AdminSchema.pre("update", updateHook);
AdminSchema.pre("findOneAndUpdate", updateHook);

AdminSchema.methods.comparePassword = function(candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
    if (err) return cb(err.message);
    cb(null, isMatch);
  });
};

AdminSchema.methods.changePassword = function(password) {
  const user = this;
  user.password = password;
  return user.save();
};

const Admin = <IAdminModel> mongoose.model("Admin", AdminSchema);
export default Admin;