function authBackendSession(req, res, next) {
  if (req.isAuthenticated()) { return next(null); }
  res.redirect("/backend");
}
export default authBackendSession;