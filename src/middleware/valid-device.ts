import Device from "../models/device";

const validDevice = async function(req, res, next) {
  const { user, deviceId } = req;

  try {
    const device = await Device.getDeviceByDeviceId(user._id, deviceId);
    if (!device) {
      return res.json({status: 499, message: "Invalid device."});
    }

    req.device = device;

    return next();
  } catch (err) {
    console.log(err);
    return res.json({status: 500, message: "Failed to validate device."});
  }
};

export default validDevice;