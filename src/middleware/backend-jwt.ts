import passport from "passport";

const authBackendJwt = function(req, res, next) {
  passport.authenticate("backend-jwt", {session: false}, function(err, admin) {
    if (err) { return res.json({status: 500, message: "Authentication failed."}); }
    if (!admin) { return res.json({status: 401, message: "Authentication unauthorized."}); }

    req.admin = admin;
    next();
  })(req, res, next);
};
export default authBackendJwt;