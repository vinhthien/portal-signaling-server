import passport = require("passport");
import { Request, Response, NextFunction } from "express";
import { IUser } from "../models/user.interface";
import { ACCOUNT_STATUSES } from "../config/user.const";

interface IPayload {
  user: IUser;
  deviceId: string;
}

const authJwt = function(req: Request, res: Response, next: NextFunction) {
  passport.authenticate("api-jwt", {session: false}, function(err, payload: IPayload) {
    const { user, deviceId } = payload;

    if (err) { return res.json({status: 500, message: "Authentication failed."}); }
    if (!user) { return res.json({status: 401, message: "Authentication unauthorized."}); }
    if (!user.verified) { return res.json({status: 401, message: "User not verified."}); }
    if (user.accountStatus === ACCOUNT_STATUSES.suspended) {
      return res.json({status: 401, message: "User is suspended."});
    }

    req.user = user;
    req.deviceId = deviceId;
    return next();
  })(req, res, next);
};

export default authJwt;