import dotenv from "dotenv";
if (process.env.NODE_ENV !== "production") {
  dotenv.config();
}

import express from "express";
import path from "path";
// import favicon from "serve-favicon";
import logger from "morgan";
import cors from "cors";
import bodyParser from "body-parser";
import { client } from "./config/redis";
import session from "express-session";
import flash from "express-flash";
import connectredis from "connect-redis";
import passport from "passport";

// db setup
import mongoose from "mongoose";
const ObjectId = mongoose.Types.ObjectId;

const mongoOptions = {
  reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
  reconnectInterval: 500, // Reconnect every 500ms
  useNewUrlParser: true,
};
(async function() {
  for (let i = 0; i < mongoOptions.reconnectTries; ++i) {
    try {
      await mongoose.connect(process.env.MONGO_URL, mongoOptions);
      console.log("Connected to MongoDB!");
      break;
    } catch (err) {
      console.log(err);
      await new Promise((resolve) => {
        setTimeout(() => (resolve(true)), mongoOptions.reconnectInterval);
      });
    }
  }
})();

const app = express();

if (process.env.PORT) app.set("port", process.env.PORT);

// sessions setup
const RedisStore = connectredis(session);
const sessionSecret = process.env.SESSION_SECRET || "weak secret";
const sessionOptions = {
  store: new RedisStore({
    client,
  }),
  secret: sessionSecret,
  cookie: { secure: false },
  resave: false,
  saveUninitialized: true, // https://github.com/expressjs/session/issues/56
};
if (process.env.NODE_ENV === "production") {
  app.set("trust proxy", 1);
  sessionOptions.cookie.secure = true;
}

app.use(session(sessionOptions));
app.use(flash());

// view engine setup
app.set("views", path.join(__dirname, "../views"));
app.set("view engine", "pug");

// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, "public", "favicon.ico")));
app.use(cors());

if (process.env.NODE_ENV !== "production") {
  app.use(logger("dev"));
}

import { BODY_PARSER_LIMIT } from "./config/server.const";
app.use(bodyParser.json({
  limit: BODY_PARSER_LIMIT,
}));
app.use(bodyParser.urlencoded({
  extended: true,
  limit: BODY_PARSER_LIMIT,
  parameterLimit: 5000,
}));

// express-session settings TODO revise later
app.use(session({
  secret: "secret",
  resave: false,
  saveUninitialized: false,
  cookie: {
    httpOnly: true,
    secure: false,
    maxAge: 30 * 60 * 1000
  }
}));

/* passport setup */
app.use(passport.initialize());
app.use(passport.session());
// app.use(cookieParser());
import { CACHE_MAX_AGE } from "./config/server.const";
app.use(express.static(path.join(__dirname, "public"), {maxAge: CACHE_MAX_AGE}));

/* passport authentication strategies */
import User from "./models/user";
import Admin from "./models/admin";
import { IAdmin } from "./models/admin.interface";
import { Strategy as LocalStrategy } from "passport-local";
passport.use("api-local", new LocalStrategy(
  {
    usernameField: "email",
  },
  function(email, password, done) {
    User.findOne({ email, verified: true }, function (err, user) {
      if (err) { return done(err); }
      if (!user) { return done(null, false); }

      user.comparePassword(password, function (err, isMatch) {
        if (err) {return done(null, false); }
        if (!isMatch) {return done(null, false); }

        return done(null, user);
      });

    });
  }
));

passport.use("backend-local", new LocalStrategy(
  {
    usernameField: "email",
  },
  function(email, password, done) {
    console.log(email);
    Admin.findOne({email}, function (err, admin) {
      if (err) { return done(err); }
      if (!admin) { return done(null, false); }

      admin.comparePassword(password, function (err, isMatch) {
        if (err) {return done(null, false); }
        if (!isMatch) {return done(null, false); }

        return done(null, admin);
      });

    });
  }
));

import { Strategy as JwtStrategy, ExtractJwt } from "passport-jwt";
import { IDevice } from "./models/device.interface";
declare global {
  namespace Express {
    interface Request {
      deviceId?: string;
      device?: IDevice;
    }
  }
}
const jwtOpts = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: process.env.JWT_SECRET,
};
passport.use("api-jwt", new JwtStrategy(jwtOpts, function(payload, done) {
  try {
    User.findOne({_id: new ObjectId(payload.id)}, function(err, user) {
      if (err) {
        return done(err, false);
      }
      if (user) {
        const { deviceId } = payload;
        return done(null, { user, deviceId });
      } else {
        return done(null, false);
      }
    });
  } catch (err) {
    return done(err, false);
  }
}));

/*** Backoffice ***/
passport.use("backend-jwt", new JwtStrategy(jwtOpts, function(payload, done) {
  try {
    Admin.findOne({_id: new ObjectId(payload.id)}, function(err, admin) {
      if (err) {
        return done(err, false);
      }
      if (admin) {
        return done(null, admin);
      } else {
        return done(null, false);
      }
    });
  } catch (err) {
    return done(err, false);
  }
}));

/* session serialization */
passport.serializeUser(function(admin: IAdmin, done) {
  done(null, admin.id);
});
passport.deserializeUser(function(id, done) {
  Admin.findById(id, function(err, admin) {
    done(err, admin);
  });
});

/* passport setup */
app.use(passport.initialize());
app.use(passport.session());

/* router configuration */
import apiRouter from "./routes/api";
import backendRouter from "./routes/backend";

app.use("/api", apiRouter);
app.use("/backend", backendRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  const err = <any> new Error("Not Found");
  err.status = 404;
  next(err);
});

// error handler
// have to keep next http://expressjs.com/en/guide/using-middleware.html#middleware.error-handling
app.use(function(err, req, res, next) { // tslint:disable-line
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

export default app;