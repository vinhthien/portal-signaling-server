import express = require("express");
import Device from "../models/device";
import authJwt from "../middleware/jwt";
import validDevice from "../middleware/valid-device";
import _findIndex from "lodash/findIndex";
import _isEmpty from "lodash/isEmpty";
import _trim from "lodash/trim";
import Contact from "../models/contact";
import { DEVICE_TYPES } from "../config/device.const";
import { NOTIFICATION_TYPE, SCREEN_TYPE } from "../config/firebase.const";
import { pushData } from "../config/firebase";

const router = express.Router();
router.use(authJwt);
/* No valid device ID needed */

/**
 * @api {get} /device/names Get Device Names
 * @apiName GetDeviceNames
 * @apiDescription Get user's device names
 * @apiGroup Device
 *
 * @apiSuccess {Number} status Status code.
 * @apiSuccess {String} message Status description.
 * @apiSuccess {String[]} deviceNames Device Names.
 *
 * @apiError 401 User unauthorized
 * @apiError (Error 5xx) 500 Internal server error.
 */
router.get("/names", async function(req, res) {
  try {
    const devices = await Device.getDevices(req.user._id);
    const deviceNames = devices.map(device => device.deviceName);
    return res.json({ status: 200, message: "Success.", deviceNames });
  } catch (err) {
    console.log(err);
    return res.json({status: 500, message: "Internal server error."});
  }
});

/**
 * @api {post} /device/associateToName Associate Device Name
 * @apiName PostDeviceAssociateToName
 * @apiDescription Associate current device to existing device name.
 * @apiGroup Device
 *
 * @apiParam {String} deviceName Desired device name to associate current device to.
 * @apiParam {String} deviceType Type of device (e.g., iPhone, iPad).
 * @apiParam {String} [pushToken] Device token for push notification.
 *
 * @apiSuccess {Number} status Status code.
 * @apiSuccess {String} message Status description.
 * @apiSuccess {Object} device Device Info
 * @apiSuccess {String} device.deviceId Device ID.
 * @apiSuccess {String} device.portalId Portal ID.
 * @apiSuccess {String} device.deviceName Device Name.
 * @apiSuccess {String} device.deviceType Device Type.
 *
 * @apiError 400 User input error.
 * @apiError 401 User unauthorized
 * @apiError (Error 5xx) 500 Internal server error.
 */
router.post("/associateToName", async function(req, res) {
  const { deviceId, user } = req;
  const { deviceName, deviceType, pushToken } = req.body;

  try {
    // validate device name
    if (_isEmpty(_trim(deviceName))) {
      return res.json({ status: 400, message: "Invalid device name." });
    }

    if (_findIndex(Object.values(DEVICE_TYPES), type => type === deviceType) < 0) {
      return res.json({ status: 400, message: "Invalid device type." });
    }

    // used to notify old device if present
    const oldDevice = await Device.getDeviceByDeviceName(user._id, deviceName);

    const device = await Device.associateDevice(user._id, deviceId, deviceName, deviceType);
    if (device) {
      if (pushToken) {
        await device.updatePushToken(pushToken);
      }

      const deviceInfo = device.getDeviceInfo();
      res.json({ status: 200, message: "Success.", device: deviceInfo });

      // send push notification to deleted device
      if (oldDevice && oldDevice.pushToken) {
        const notification = {
          body: `This device has been taken over by another device.`,
          type: NOTIFICATION_TYPE.InvalidDevice,
          screenToOpen: SCREEN_TYPE.LoginScreen,
        };
        await pushData(notification, oldDevice.pushToken);
      }

      return;
    } else {
      return res.json({ status: 400, message: "Invalid device name" });
    }
  } catch (err) {
    console.log(err);
    return res.json({status: 500, message: "Internal server error."});
  }
});

/* Valid device ID required */
router.use(validDevice);

/**
 * @api {delete} /device/:portalId Delete Device
 * @apiName DeleteDevice
 * @apiDescription Delete any user device.
 * @apiGroup Device
 *
 * @apiParam {String} portalId Portal ID of device to delete
 *
 * @apiSuccess {Number} status Status code.
 * @apiSuccess {String} message Status description.
 *
 * @apiError 400 Could not find device.
 * @apiError 401 User unauthorized
 * @apiError 499 Invalid Device
 * @apiError (Error 5xx) 500 Internal server error.
 */
router.delete("/:portalId", async function(req, res) {
  const { portalId } = req.params;
  const { user } = req;

  try {
    const targetDevice = await Device.findDevice(portalId);
    if (!targetDevice || targetDevice.userId.toString() !== req.user.id) {
      return res.json({ status: 400, message: "Invalid portal ID"});
    }

    const isDeleted = await Device.removeDevice(user._id, targetDevice.deviceId);

    if (isDeleted) {
      await Contact.removeDeviceContacts(portalId);
      res.json({ status: 200, message: "Success." });

      // send push notification to deleted device
      if (targetDevice.pushToken) {
        const notification = {
          body: `This device has been deleted.`,
          type: NOTIFICATION_TYPE.InvalidDevice,
          screenToOpen: SCREEN_TYPE.LoginScreen,
        };
        await pushData(notification, targetDevice.pushToken);
      }

      return;
    } else {
      // this should never happen in theory since the middleware checks for a valid device
      return res.json({ status: 499, message: "Invalid device." });
    }
  } catch (err) {
    console.log(err);
    return res.json({status: 500, message: "Internal server error."});
  }
});

/**
 * @api {post} /device/editName Edit Device Name
 * @apiName PostDeviceEditName
 * @apiDescription Edit current device name.
 * @apiGroup Device
 *
 * @apiParam {String} deviceName New device name for target device.
 *
 * @apiSuccess {Number} status Status code.
 * @apiSuccess {String} message Status description.
 * @apiSuccess {Object} device Device Info
 * @apiSuccess {String} device.deviceId Device ID.
 * @apiSuccess {String} device.portalId Portal ID.
 * @apiSuccess {String} device.deviceName Device Name.
 * @apiSuccess {String} device.deviceType Device Type.
 *
 * @apiError 400 Duplicate device name.
 * @apiError 400 User input error.
 * @apiError 401 User unauthorized
 * @apiError 499 Invalid Device
 * @apiError (Error 5xx) 500 Internal server error.
 */
router.post("/editName", async function(req, res) {
  const { deviceId, user } = req;
  const { deviceName, portalId } = req.body;

  try {
    // validate device name
    if (_isEmpty(_trim(deviceName))) {
      return res.json({ status: 400, message: "Invalid device name." });
    }
    console.log(44888);
  console.log(user._id);
      console.log(deviceId);
      console.log(portalId);
      console.log(deviceName);
    const device = await Device.renameDevice(user._id, portalId, deviceName);
    console.log(device);
    if (device) {
      const deviceInfo = device.getDeviceInfo();
      return res.json({ status: 200, message: "Success.", device: deviceInfo });
    } else {
      // this should never happen in theory since the middleware checks for a valid device/device name
      return res.json({ status: 499, message: "Invalid device." });
    }
  } catch (err) {
    console.log(err);
    if (typeof err !== "undefined" && err.code === 11000) { // mongodb duplicate unique key code
      if (typeof err.message === "string" && err.message.includes("deviceName")) {
        return res.json({status: 400, message: "You already has a device with this name."});
      }
    }

    return res.json({status: 500, message: "Internal server error."});
  }
});

/**
 * @api {get} /device/list Get User's Devices
 * @apiName GetDeviceList
 * @apiDescription Get info for all of user's devices
 * @apiGroup Device
 *
 * @apiSuccess {Number} status Status code.
 * @apiSuccess {String} message Status description.
 * @apiSuccess {Object[]} devices Devices' Info
 * @apiSuccess {String} devices.deviceId Device ID.
 * @apiSuccess {String} devices.portalId Portal ID.
 * @apiSuccess {String} devices.deviceName Device Name.
 * @apiSuccess {String} devices.deviceType Device Type.
 *
 * @apiError 401 User unauthorized
 * @apiError 499 Invalid Device
 * @apiError (Error 5xx) 500 Internal server error.
 */
router.get("/list", async function(req, res) {
  try {
    const devices = await Device.getDevices(req.user._id);
    return res.json({ status: 200, message: "Success.", devices });
  } catch (err) {
    console.log(err);
    return res.json({status: 500, message: "Internal server error."});
  }
});

export default router;