import socketio from "socket.io";
import _findIndex from "lodash/findIndex";
import {
  roomMatched, createNewRoom, inviteRoom, joinRoom,
  rejectRoom, acceptContact,
  addContact, rejectContact, exitAllRoom, sendPhotoUpdateRoom,
  sendPhotoModeChangedRoom
} from "../config/signaling.utils";
import Device from "../models/device";
import { PRESENCE_STATUS } from "../config/device.const";


let io;
const devices = [];
const rooms = [];

function onConnection(socket) {

  /**
   * @Name submitDeviceInfo
   * @Description When a socket connection established, devices will submit their info.
   *
   * @Param {String} portalId
   * @Param {JSON} empty
   *
   */
  socket.on("submitDeviceInfo", async function (myPortalId, data) {
    try {
      const connectedDevice = await Device.updatePresenceStatus(myPortalId, PRESENCE_STATUS.online, socket.id);
      console.debug("New device connected!");
    } catch (err) {
        console.log(err);
        console.debug("The device disconnected!");
        const connectedDevice = await Device.updatePresenceStatus(myPortalId, PRESENCE_STATUS.offline, "");
        return;
    }
  });


  /**
   * @Name inviteOrJoinRoom
   * @Description If room doesn't exist, create new room and send a invite-call request. Otherwise, join the room.
   *
   * @Param {String} portalId
   * @Param {JSON} data includes: callee_portal_id, caller_portal_id
   *
   */
  socket.on("inviteOrJoinRoom", async function (myPortalId, data) {
    const matchedRoomId = await roomMatched(myPortalId, data.callee_portal_id);

    if (matchedRoomId === undefined || matchedRoomId === null) {
      const roomId = createNewRoom(io, myPortalId, data.callee_portal_id);
      await inviteRoom(io, roomId, myPortalId, data.callee_portal_id);
    } else {
      await joinRoom(io, matchedRoomId, myPortalId, data.caller_portal_id);
    }
  });

  /**
   * @Name answerRoom
   * @Description Accept/Reject answer to invite-call request.
   *
   * @Param {String} portalId
   * @Param {JSON} data includes: room_id, caller_portal_id, answer_message
   *
   */
  socket.on("answerRoom", async function (myPortalId, data) { // room_id, caller_portal_id, answer_message
    if (data.answer_message === "AGREED") {
      await joinRoom(io, data.room_id, myPortalId, data.callee_portal_id);
    } else if (data.answer_message === "REJECTED") {
      await rejectRoom(io, data.room_id, myPortalId, data.caller_portal_id);
    }

  });

  /**
   * @Name addContact
   * @Description Send an add-new-contact request
   *
   * @Param {String} portalId
   * @Param {JSON} data includes: room_id, caller_portal_id, answer_message
   *
   */
  socket.on("addContact", async function (myPortalId, data) {
    await addContact(io, myPortalId, data.portal_id);
  });

  /**
   * @Name answerContact
   * @Description Accept/Reject answer to add contact request.
   *
   * @Param {String} portalId
   * @Param {JSON} data includes: room_id, caller_portal_id, answer_message
   *
   */
  socket.on("answerContact", async function (myPortalId, data) {
    if (data.answer_message === "AGREED") {
      await acceptContact(io, myPortalId, data.portal_id);
    } else if (data.answer_message === "REJECTED") {
      await rejectContact(io, myPortalId, data.portal_id);
    }

  });

  /**
   * @Name sendPhotoUpdate
   * @Description Fired when a device is in Idle mode and send single photo to roommates every X second(s).
   *
   * @Param {String} portalId
   * @Param {JSON} data includes: room_id, photo_base64
   *
   */
  socket.on("sendPhotoUpdate", async function (myPortalId, data) {
    await sendPhotoUpdateRoom(io, myPortalId, data.room_id, data.photo_base64);

  });

  /**
   * @Name sendPhotoModeChanged
   * @Description Fired when a device is in Idle/Active mode. This event will notify the roommates that someone in Idle/Active mode.
   *
   * @Param {String} portalId
   * @Param {JSON} data includes: room_id, active
   *
   */
  socket.on("sendPhotoModeChanged", async function (myPortalId, data) { // { }
    await sendPhotoModeChangedRoom(io, myPortalId, data.room_id, data.active);
  });

  /**
   * @Name exitRoom
   * @Description Fired when a device exit room and notify roommates that he exited.
   *
   * @Param {String} portalId
   * @Param {JSON} no data
   *
   */
  socket.on("exitRoom", async function (myPortalId, data) {
    await exitAllRoom(io, myPortalId);
  });

  /**
   * @Name sendMessage
   * @Description Fired when a device want to send RTC messages to another device
   *
   * @Param {String} portalId
   * @Param {JSON} data includes: room_id, rtc_message
   *
   */
  socket.on("sendMessage", function (device_id, data) { // { room_id, rtc_message }
    const roomIndex = _findIndex(rooms, {room_id: data.room_id});
    if (roomIndex === -1) {
      // This room is NOT available
      return;
    }

    const room = rooms[roomIndex];
    room.devices.forEach(function (device) {
      if (device.device_id === device_id) {
        // don't send myself
        return;
      }

      io.to(device.socket_id)
        .emit("messageReceived", device.device_id, data);
    });
  });

  /**
   * @Name disconnect
   * @Description fired when a socket connection is closed. We marked this device as offline.
   *
   * @Param {String} portalId
   * @Param {JSON} data includes: room_id, rtc_message
   *
   */
  socket.on("disconnect", function () {
    const index = _findIndex(devices, {socket: socket.id});
    if (index !== -1) {
      socket.broadcast.emit("offline", devices[index].device_id);
      console.debug("Device \"" + devices[index].device_id + "\" disconnected");
      devices.splice(index, 1);
      console.debug("Number of connected devices: " + devices.length);
    }
  });
}

export default function (listener) {
  io = socketio(listener, {
    serveClient: false,
  });

  io.on("connection", onConnection);
  return io;
}