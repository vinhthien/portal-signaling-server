import express from "express";
import sizeOf from "image-size";
import _isEmpty from "lodash/isEmpty";
import _trim from "lodash/trim";
import authJwt from "../middleware/jwt";
import validDevice from "../middleware/valid-device";
import Device from "../models/device";
import { decodeBase64Image } from "../config/image.utils";
import { AVATAR_DIMS } from "../config/user.const";

const router = express.Router();

router.use(authJwt);
router.use(validDevice);

/**
 * @api {put} /user/avatar Update User Avatar
 * @apiName PutUserAvatar
 * @apiGroup Auth
 *
 * @apiParam {String} avatarData User's avatar image (base64 encoding).
 *
 * @apiSuccess {Number} status Status code.
 * @apiSuccess {String} message Status description.
 * @apiSuccess {String} avatarUrl New Avatar URL.
 *
 * @apiError 400 User input error.
 * @apiError 401 User unauthorized
 * @apiError 499 Invalid Device
 * @apiError (Error 5xx) 500 Internal server error.
 */
router.put("/avatar", async function(req, res) {
  let { user } = req;
  const { avatarData } = req.body;
  try {
    // validate avatar
    if (!avatarData) {
      return res.json({status: 400, message: "Please provide a valid avatar image."});
    }
    const image = decodeBase64Image(avatarData);
    const dimensions = sizeOf(image.data);
    if (dimensions.width / dimensions.height !== AVATAR_DIMS.width / AVATAR_DIMS.height) {
      return res.json({status: 400, message: `Avatar image dimensions not ${AVATAR_DIMS.width}:${AVATAR_DIMS.height}.`});
    }

    try {
      user = await user.updateAvatar(avatarData);
    } catch (err) {
      if (err === 500) {
        return res.json({status: 500, message: "Failed to save avatar image."});
      } else if (err === 400) {
        return res.json({status: 400, message: "Please provide a valid avatar image."});
      } else {
        throw err;
      }
    }

    const avatarUrl = user.getAvatarImage(req);
    return res.json({ status: 200, message: "Success.", avatarUrl });
  } catch (err) {
    console.log(err);
    return res.json({status: 500, message: "Internal server error."});
  }
});

/**
 * @api {put} /user/username Update Username
 * @apiName PutUsername
 * @apiGroup Auth
 *
 * @apiParam {String} username User's account name.
 *
 * @apiSuccess {Number} status Status code.
 * @apiSuccess {String} message Status description.
 *
 * @apiError 400 User input error.
 * @apiError 401 User unauthorized
 * @apiError 499 Invalid Device
 * @apiError (Error 5xx) 500 Internal server error.
 */
router.put("/username", async function(req, res) {
  const { user } = req;
  const { username } = req.body;
  try {
    // validate avatar
    if (_isEmpty(_trim(username))) {
      return res.json({status: 400, message: "Please provide a valid username."});
    }

    await user.updateUsername(username);
    await Device.updateDevicesForUser(user._id, { username });
    return res.json({ status: 200, message: "Success." });
  } catch (err) {
    console.log(err);
    return res.json({status: 500, message: "Internal server error."});
  }
});

export default router;