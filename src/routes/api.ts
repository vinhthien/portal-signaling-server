import express = require("express");

import auth from "./auth";
import contact from "./contact";
import device from "./device";
import user from "./user";

const router = express.Router();

router.use("/auth", auth);
router.use("/contact", contact);
router.use("/device", device);
router.use("/user", user);

export default router;