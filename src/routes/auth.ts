import express from "express";
import passport from "passport";
import _isEmpty from "lodash/isEmpty";
import _trim from "lodash/trim";
import sizeOf from "image-size";
import User from "../models/user";
import Device from "../models/device";
import { IUser } from "../models/user.interface";
import authJwt from "../middleware/jwt";
import { PW_RE } from "../config/auth.regex";
import { makeJwtToken } from "../config/auth.util";
import { sendText } from "../config/email";
import { CS_EMAIL } from "../config/email.const";
import { decodeBase64Image } from "../config/image.utils";
import { AVATAR_DIMS, ACCOUNT_STATUSES } from "../config/user.const";

const router = express.Router();

/**
 * @api {post} /auth/login Login
 * @apiName PostAuthLogin
 * @apiGroup Auth
 *
 * @apiParam {String} email User's email.
 * @apiParam {String} password User's password.
 * @apiParam {String} deviceId Device's unique ID (identifierForVendor).
 * @apiParam {String} [pushToken] Device token for push notification.
 *
 * @apiSuccess {Number} status Status code.
 * @apiSuccess {String} message Status description.
 * @apiSuccess {String} token JWT token.
 * @apiSuccess {Boolean} deviceFound Whether or not device corresponding to deviceId was found.
 * @apiSuccess {Object} device User device information (only available if deviceFound = true).
 * @apiSuccess {String} device.portalId Device's Portal ID (unique)
 * @apiSuccess {String} device.deviceId Device's Device ID (should be unique, but is used in conjunction with User ID for safety)
 * @apiSuccess {String} device.deviceName Device's user-assigned name
 * @apiSuccess {String} device.deviceType Device's type
 * @apiSuccess {Object} account User account information.
 * @apiSuccess {String} account.email User's email.
 * @apiSuccess {String} account.username User's username.
 * @apiSuccess {String} account.avatarUrl User's avatar's URL
 *
 * @apiError 400 User input error (deviceId is invalid).
 * @apiError 401 User unauthorized (email, or password is incorrect).
 * @apiError (Error 5xx) 500 Internal server error.
 */
router.post("/login", function(req, res, next) {
  const { deviceId, pushToken } = req.body;
  if (_isEmpty(_trim(deviceId))) {
    return res.json({status: 400, message: "Please supply device ID."});
  }

  passport.authenticate("api-local", async function(err, user: IUser) {
    if (err) { return res.json({status: 500, message: "Login failed."}); }
    if (!user) { return res.json({status: 401, message: "Login unauthorized."}); }
    if (!user.verified) { return res.json({status: 401, message: "User not verified."}); }
    if (user.accountStatus === ACCOUNT_STATUSES.suspended) {
      return res.json({status: 401, message: "User is suspended."});
    }

    // check if deviceId already exists, if not, let the user know
    const userId = user._id;

    try {
      const device = await Device.findOne({ deviceId, userId });
      const deviceFound = !!device;

      if (deviceFound && pushToken) {
        await device.updatePushToken(pushToken);
      }

      const token = makeJwtToken({
        id: user.id,
        deviceId,
      });
      if (!token) throw "Failed to create JWT token";

      const account = user.getAccountSummary(req);
      const deviceInfo = device ? device.getDeviceInfo() : undefined;

      return res.json({
        status: 200,
        message: "Login succeeded.",
        token,
        deviceFound,
        device: deviceInfo,
        account,
      });
    } catch (err) {
      console.log(err);
      return res.json({status: 500, message: "Internal Server Error."});
    }
  })(req, res, next);
});

/**
 * @api {post} /auth/register Register
 * @apiName PostAuthRegister
 * @apiGroup Auth
 *
 * @apiParam {String} email User's email.
 * @apiParam {String} password User's password.
 * @apiParam {String} username User's username.
 * @apiParam {String} avatarData User's avatar image (base64 encoding).
 *
 * @apiSuccess {Number} status Status code.
 * @apiSuccess {String} message Status description.
 * @apiSuccess {Object} account User account information.
 * @apiSuccess {String} account.email User's email.
 * @apiSuccess {String} account.username User's username.
 * @apiSuccess {String} account.avatarUrl User's avatar's URL
 *
 * @apiError 400 Duplicate user.
 * @apiError 400 User input error.
 * @apiError (Error 5xx) 500 Internal server error.
 */
router.post("/register", async function(req, res) {
  const { email, password, username, avatarData } = req.body;

  try {
    // validate password
    if (!PW_RE.test(password)) {
      return res.json({status: 400, message: "Please input valid password."});
    }

    // validate user name
    if (_isEmpty(_trim(username))) {
      return res.json({status: 400, message: "Please input valid username."});
    }

    // validate avatar
    if (!avatarData) {
      return res.json({status: 400, message: "Please provide a valid avatar image."});
    }
    const image = decodeBase64Image(avatarData);
    const dimensions = sizeOf(image.data);
    if (dimensions.width / dimensions.height !== AVATAR_DIMS.width / AVATAR_DIMS.height) {
      return res.json({status: 400, message: `Avatar image dimensions not ${AVATAR_DIMS.width}:${AVATAR_DIMS.height}.`});
    }

    // create user!
    let user = await User.registerUser(email, password, username);
    if (!user) {
      return res.json({status: 400, message: "User already exists."});
    }

    try {
      user = await user.updateAvatar(avatarData);
    } catch (err) {
      console.log(err);
      if (err === 500) {
        return res.json({status: 500, message: "Failed to save avatar image."});
      } else if (err === 400) {
        return res.json({status: 400, message: "Please provide a valid avatar image."});
      } else {
        throw err;
      }
    }

    const code = await user.makeVerificationCode();
    const from = CS_EMAIL;
    const to = email;
    const subject = "<The Portal> Registration Verification Code";
    const message = `Thank you for choosing Entinux service.

    Your registration verification code for <The Portal> is ${code}.

    Having issues? Please file us a ticket at helpdesk.entinux.com, select category - [The Portal].`;

    await sendText(from, to, subject, message);

    const account = user.getAccountSummary(req);

    return res.json({status: 200, message: "Success", account});
  } catch (err) {
    console.log(err);
    if (typeof err !== "undefined" && err.code === 11000) { // mongodb duplicate unique key code
      if (typeof err.message === "string" && err.message.includes("email")) {
        return res.json({status: 400, message: "Inputted email is already registered."});
      }
    }

    return res.json({status: 500, message: "Internal server error."});
  }
});

/**
 * @api {post} /auth/verify Verify Code
 * @apiName PostAuthVerify
 * @apiDescription This will finish registration and verify the user.
 * @apiGroup Auth
 *
 * @apiParam {String} email User's email.
 * @apiParam {String} code Verification code received by email.
 *
 * @apiSuccess {Number} status Status code.
 * @apiSuccess {String} message Status description.
 *
 * @apiError 400 User input error.
 * @apiError (Error 5xx) 500 Internal server error.
 */
router.post("/verify", async function(req, res) {
  const { email, code } = req.body;

  try {
    const user = await User.verifyUser(email, code);

    if (user) {
      return res.json({status: 200, message: "Success."});
    } else {
      return res.json({status: 400, message: "Invalid input."});
    }
  } catch (err) {
    console.log(err);
    return res.json({status: 500, message: "Internal server error."});
  }
});

/**
 * @api {post} /auth/resetpassword/start Start Reset Password Process
 * @apiName PostAuthResetpasswordStart
 * @apiDescription This will send an email with verification code to user's email address.
 * @apiGroup Auth
 *
 * @apiParam {String} email User's email.
 *
 * @apiSuccess {Number} status Status code.
 * @apiSuccess {String} message Status description.
 *
 * @apiError 400 User input error.
 * @apiError (Error 5xx) 500 Internal server error.
 */
router.post("/resetpassword/start", async function(req, res) {
  const { email } = req.body;

  try {
    const user = await User.findOne({ email, verified: true });
    if (!user) {
      return res.json({status: 400, message: "Invalid email address"});
    }

    const code = await user.makeVerificationCode();
    const from = CS_EMAIL;
    const to = email;
    const subject = "<The Portal> Reset Password Verification Code";
    const message = `Thank you for choosing Entinux service.

    Your verification code for <The Portal> is ${code}.

    Having issues? Please file us a ticket at helpdesk.entinux.com, select category - [The Portal].`;

    await sendText(from, to, subject, message);

    return res.json({status: 200, message: "Success"});
  } catch (err) {
    console.log(err);
    return res.json({status: 500, message: "Internal server error."});
  }
});

/**
 * @apiDefine CheckVerifyCode
 *
 * @apiParam {String} email User's email.
 * @apiParam {String} code Verification code received by email.
 *
 * @apiSuccess {Number} status Status code.
 * @apiSuccess {String} message Status description.
 *
 * @apiError 400 User input error.
 * @apiError (Error 5xx) 500 Internal server error.
 */
/**
 * @api {post} /auth/resetpassword/check Check Reset Password Verification Code
 * @apiName PostAuthResetpasswordCheck
 * @apiDescription This can be used during password reset to check the code.
 * @apiGroup Auth
 *
 * @apiUse CheckVerifyCode
 */
/**
 * @api {post} /auth/verify/check Check Registration Verification Code
 * @apiName PostAuthVerifyCheck
 * @apiDescription This can be used during registration verification to check the code.
 * @apiGroup Auth
 *
 * @apiUse CheckVerifyCode
 */
router.post(["/resetpassword/check", "/verify/check"], async function(req, res) {
  const { email, code } = req.body;

  try {
    const user = await User.checkVerificationCode(email, code);

    if (user) {
      return res.json({status: 200, message: "Success."});
    } else {
      return res.json({status: 400, message: "Invalid input."});
    }
  } catch (err) {
    console.log(err);
    return res.json({status: 500, message: "Internal server error."});
  }
});

/**
 * @api {post} /auth/resetpassword/end Finish Reset Password Process
 * @apiName PostAuthResetpasswordEnd
 * @apiDescription Set new user-defined password if code is correct.
 * @apiGroup Auth
 *
 * @apiParam {String} email User's email.
 * @apiParam {String} password User's password.
 * @apiParam {String} code Verification code received by email.
 *
 * @apiSuccess {Number} status Status code.
 * @apiSuccess {String} message Status description.
 *
 * @apiError 400 User input error.
 * @apiError (Error 5xx) 500 Internal server error.
 */
router.post("/resetpassword/end", async function(req, res) {
  const { email, code, password } = req.body;

  try {
    // validate password
    if (!PW_RE.test(password)) {
      return res.json({status: 400, message: "Please input valid password."});
    }

    // check for valid code
    const user = await User.checkVerificationCode(email, code);
    if (!user) {
      return res.json({status: 400, message: "Invalid input."});
    }

    // reset password
    await user.changePassword<IUser>(password);
    return res.json({status: 200, message: "Success."});
  } catch (err) {
    console.log(err);
    return res.json({status: 500, message: "Internal server error."});
  }
});

/* secure routes */
router.use(authJwt);

/**
 * @api {logout} /auth/logout Logout
 * @apiName PostAuthLogout
 * @apiDescription Side effects: Push notification token is removed.
 * @apiGroup Auth
 *
 * @apiSuccess {Number} status Status code.
 * @apiSuccess {String} message Status description.
 *
 * @apiError 401 User unauthorized
 * @apiError (Error 5xx) 500 Internal server error.
 */
router.post("/logout", async function(req, res) {
  const { deviceId } = req;
  const { _id } = req.user;
  try {
    await Device.removePushToken(_id, deviceId);

    return res.json({status: 200, message: "Success."});
  } catch (err) {
    console.log(err);
    return res.json({status: 500, message: "Internal server error."});
  }
});

/**
 * @api {post} /auth/changepassword Change Password
 * @apiName PostAuthChangepassword
 * @apiDescription Set a new user password while logged in.
 * @apiGroup Auth
 *
 * @apiParam {String} newPassword User's new password.
 * @apiParam {String} oldPassword User's old password.
 *
 * @apiSuccess {Number} status Status code.
 * @apiSuccess {String} message Status description.
 *
 * @apiError 400 User input error.
 * @apiError 401 User unauthorized
 * @apiError (Error 5xx) 500 Internal server error.
 */
router.post("/changepassword", async function(req, res) {
  const { newPassword, oldPassword } = req.body;
  const { user }: { user?: IUser } = req;

  try {
    // validate password
    if (!PW_RE.test(newPassword)) {
      return res.json({status: 400, message: "Please input valid password."});
    }

    // check old password
    const isCorrectOldPassword = await new Promise(resolve => {
      user.comparePassword(oldPassword, function (err, isMatch) {
        if (err) { return resolve(false); }
        if (!isMatch) { return resolve(false); }

        return resolve(true);
      });
    });
    if (!isCorrectOldPassword) {
      return res.json({status: 400, message: "Please input correct old password."});
    }

    // change password
    await user.changePassword<IUser>(newPassword);
    return res.json({status: 200, message: "Success."});
  } catch (err) {
    console.log(err);
    return res.json({status: 500, message: "Internal server error."});
  }
});

export default router;