import express from "express";
import passport from "passport";
import authBackendSession from "../middleware/backend-session";
import membershipRouter from "./backend-membership";
import paymenHistoryRouter from "./backend-payment-history";

const router = express.Router();

router.use("/membership", membershipRouter);
router.use("/payment-history", paymenHistoryRouter);

router.post("/login", passport.authenticate("backend-local", {
  successRedirect: "/backend/dashboard",
  failureRedirect: "/backend",
  failureFlash: true
}));

router.get("/dashboard", authBackendSession, function (req, res) {
  const { username } = req.user;

  return res.render("dashboard", {
    username,
  });
});

router.get("/", function (req, res) {
  if (req.isAuthenticated()) { return res.redirect("/backend/dashboard"); }

  return res.render("login", {title: "Login"});
});

export default router;
