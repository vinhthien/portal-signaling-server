import express from "express";
import mongoose from "mongoose";
import authJwt from "../middleware/jwt";
import validDevice from "../middleware/valid-device";
import Contact from "../models/contact";
import { ContactStatus } from "../models/contact.interface";
import Device from "../models/device";
import { pushData } from "../config/firebase";
import { NOTIFICATION_TYPE, SCREEN_TYPE } from "../config/firebase.const";

const router = express.Router();

router.use(authJwt);
router.use(validDevice);

/**
 * @api {post} /contact/add Add Contact
 * @apiName PostContactAdd
 * @apiDescription Makes a request to add contact
 * @apiGroup Contact
 *
 * @apiParam {String} portalId Desired contact's portalId.
 *
 * @apiSuccess {Number} status Status code.
 * @apiSuccess {String} message Status description.
 *
 * @apiError 400 User input error.
 * @apiError 401 User unauthorized.
 * @apiError 402 The target blocked you
 * @apiError 403 Duplicate sending request.
 * @apiError 404 The Portal ID is on blocked list
 * @apiError 499 Invalid Device
 * @apiError (Error 5xx) 500 Internal server error.
 */
router.post("/add", async function(req, res) {
  const { portalId: targetPortalId } = req.body;
  const { device: userDevice } = req;
  const { username } = req.user;

  try {
    const targetDevice = await Device.findDevice(targetPortalId);
    if (!targetDevice) {
      return res.json({status: 400, message: "Invalid Portal ID."});
    }

    // do not add if you are blocked
    const targetContact = await Contact.getContact(targetPortalId, userDevice.portalId);

    if (targetContact && targetContact.status === ContactStatus.blocked) {
      return res.json({status: 402, message: "The target blocked you"});
    }

    const myContact = await Contact.getContact(userDevice.portalId, targetPortalId);
    if (myContact && myContact.status === ContactStatus.blocked) {
        return res.json({status: 404, message: "The Portal ID is on your Block list."});
    }

    // do not add if you are waiting for response
    if (myContact && myContact.status === ContactStatus.pending) {
        return res.json({status: 403, message: "Duplicate sending request."});
    }

    await Contact.addContact(userDevice, targetDevice);

    res.json({status: 200, message: "Success."});

    // send push notification to target
    if (targetDevice.pushToken) {
      const notification = {
        body: `${userDevice.deviceName} (${username}) would like to add you as a contact.`,
        type: NOTIFICATION_TYPE.AddContact,
        screenToOpen: SCREEN_TYPE.ContactScreen,
      };
      await pushData(notification, targetDevice.pushToken);
    }

    return;
  } catch (err) {
    console.log(err);

    if (typeof err !== "undefined" && err.code === 11000) { // mongodb duplicate unique key code
      return res.json({status: 400, message: "You already has a device with this name."});
    }

    return res.json({status: 500, message: "Internal server error."});
  }
});

/**
 * @api {post} /contact/accept Accept Contact
 * @apiName PostContactAccept
 * @apiDescription Accept request to add contact
 * @apiGroup Contact
 *
 * @apiParam {String} portalId Desired contact's portalId.
 *
 * @apiSuccess {Number} status Status code.
 * @apiSuccess {String} message Status description.
 *
 * @apiError 400 User input error.
 * @apiError 401 User unauthorized
 * @apiError 499 Invalid Device
 * @apiError (Error 5xx) 500 Internal server error.
 */
router.post("/accept", async function(req, res) {
  const { portalId: targetPortalId } = req.body;
  const { device: userDevice } = req;
  const { username } = req.user;

  try {
    const targetDevice = await Device.findDevice(targetPortalId);
    if (!targetDevice) {
      return res.json({status: 400, message: "Invalid Portal ID."});
    }

    await Contact.acceptContact(userDevice.portalId, targetPortalId);
    res.json({status: 200, message: "Success."});

    // send push notification to target
    if (targetDevice.pushToken) {
      const notification = {
        body: `${userDevice.deviceName} (${username}) has accepted your request to add as contact.`,
        type: NOTIFICATION_TYPE.AcceptContact,
        screenToOpen: SCREEN_TYPE.ContactScreen,
      };
      await pushData(notification, targetDevice.pushToken);
    }

    return;
  } catch (err) {
    console.log(err);
    return res.json({status: 500, message: "Internal server error."});
  }
});

/**
 * @api {put} /contact/autoAccept Set Contact to Auto-Accept
 * @apiName PutContactAutoAccept
 * @apiDescription Set contact auto-accept setting
 * @apiGroup Contact
 *
 * @apiParam {String} portalId Desired contact's portalId.
 * @apiParam {Boolean} autoAccept Auto-accept setting (true = "on", false = "off").
 *
 * @apiSuccess {Number} status Status code.
 * @apiSuccess {String} message Status description.
 *
 * @apiError 400 User input error.
 * @apiError 401 User unauthorized
 * @apiError 499 Invalid Device
 * @apiError (Error 5xx) 500 Internal server error.
 */
router.put("/autoAccept", async function(req, res) {
  const { portalId: targetPortalId, autoAccept } = req.body;
  const { device: userDevice } = req;

  try {
    const device = await Device.findDevice(targetPortalId);
    if (!device) {
      return res.json({status: 400, message: "Invalid Portal ID."});
    }

    const autoAcceptBool = autoAccept === "on" ? true : false;
    const contact = await Contact.setAutoAccept(userDevice.portalId, targetPortalId, autoAcceptBool);
    if (contact) {
      return res.json({status: 200, message: "Success."});
    } else {
      return res.json({status: 400, message: "Invalid contact."});
    }
  } catch (err) {
    console.log(err);

    return res.json({status: 500, message: "Internal server error."});
  }
});

/**
 * @api {post} /contact/block Block Contact
 * @apiName PostContactBlock
 * @apiGroup Contact
 *
 * @apiParam {String} portalId Desired contact's portalId.
 *
 * @apiSuccess {Number} status Status code.
 * @apiSuccess {String} message Status description.
 *
 * @apiError 400 User input error.
 * @apiError 401 User unauthorized
 * @apiError 499 Invalid Device
 * @apiError (Error 5xx) 500 Internal server error.
 */
router.post("/block", async function(req, res) {
  const { device: userDevice } = req;
  const { portalId: targetPortalId } = req.body;

  try {
    const targetDevice = await Device.findDevice(targetPortalId);
    if (!targetDevice) {
      return res.json({status: 400, message: "Invalid Portal ID."});
    }

    const { portalId: userPortalId } = userDevice;

    // consider refactoring blockContact to handle both cases
    const contact = await Contact.blockContact(userPortalId, targetPortalId);
    if (!contact) {
      await Contact.addContact(userDevice, targetDevice, true);
    }

    return res.json({status: 200, message: "Success."});
  } catch (err) {
    console.log(err);

    if (typeof err !== "undefined" && err.code === 11000) { // mongodb duplicate unique key code
      return res.json({status: 400, message: "You already has a device with this name."});
    }

    return res.json({status: 500, message: "Internal server error."});
  }
});

/**
 * @api {get} /contact/list Get All Contacts
 * @apiName GetContactList
 * @apiDescription Lists contact list, incoming requests, and blocked contacts
 * @apiGroup Contact
 *
 * @apiSuccess {Number} status Status code.
 * @apiSuccess {String} message Status description.
 * @apiSuccess {Object[]} contacts Contact List.
 * @apiSuccess {String} contacts.portalId Contact's Portal ID.
 * @apiSuccess {String} contacts.deviceName Contact's Device Name.
 * @apiSuccess {String} contacts.username Contact's Display Name.
 * @apiSuccess {Boolean} contacts.autoAccept Contact's Auto-Accept Status.
 * @apiSuccess {Object[]} requests Incoming Contact Request List.
 * @apiSuccess {String} requests.portalId Requester's Portal ID.
 * @apiSuccess {String} requests.deviceName Requester's Device Name.
 * @apiSuccess {String} requests.username Contact's Display Name.
 * @apiSuccess {Object[]} blocked Blocked Contact List.
 * @apiSuccess {String} blocked.portalId Blocked Contact's Portal ID.
 * @apiSuccess {String} blocked.deviceName Blocked Contact's Device Name.
 * @apiSuccess {String} blocked.username Blocked Contact's Display Name.
 *
 * @apiError 401 User unauthorized
 * @apiError 499 Invalid Device
 * @apiError (Error 5xx) 500 Internal server error.
 */
router.get("/list", async function (req, res) {
  const { device } = req;
  try {
    const userId = req.user._id;

    const [ contacts, requests, blocked ] = await Promise.all([
      Contact.getContacts(device.portalId),
      Contact.getContactRequests(device.portalId),
      Contact.getBlockedContacts(device.portalId),
    ]);

    return res.json({
      status: 200,
      message: "Success.",
      contacts,
      requests,
      blocked,
    });
  } catch (err) {
    console.log(err);
    return res.json({status: 500, message: "Internal server error."});
  }
});

/**
 * @api {post} /contact/reject Reject Contact
 * @apiName PostContactReject
 * @apiGroup Contact
 *
 * @apiParam {String} portalId Desired contact's portalId.
 *
 * @apiSuccess {Number} status Status code.
 * @apiSuccess {String} message Status description.
 *
 * @apiError 400 User input error.
 * @apiError 401 User unauthorized
 * @apiError 499 Invalid Device
 * @apiError (Error 5xx) 500 Internal server error.
 */
router.post("/reject", async function(req, res) {
  const { portalId: targetPortalId } = req.body;
  const { device: userDevice } = req;

  try {
    const targetDevice = await Device.findDevice(targetPortalId);
    if (!targetDevice) {
      return res.json({status: 400, message: "Invalid Portal ID."});
    }

    await Contact.rejectContact(userDevice.portalId, targetPortalId);
    return res.json({status: 200, message: "Success."});
  } catch (err) {
    console.log(err);
    return res.json({status: 500, message: "Internal server error."});
  }
});

/**
 * @apiDefine RemoveContact
 *
 * @apiParam {String} portalId Desired contact's portalId.
 *
 * @apiSuccess {Number} status Status code.
 * @apiSuccess {String} message Status description.
 *
 * @apiError 400 User input error.
 * @apiError 401 User unauthorized
 * @apiError 499 Invalid Device
 * @apiError (Error 5xx) 500 Internal server error.
 */
/**
 * @api {post} /contact/remove Remove Contact
 * @apiName PostContactRemove
 * @apiDescription This is used to remove a contact from the normal contact list.
 * @apiGroup Contact
 *
 * @apiUse RemoveContact
 */
/**
 * @api {post} /contact/block/remove Remove Blocked Contact
 * @apiName PostContactBlockRemove
 * @apiDescription This is used to remove a contact from the blocked contact list.
 * @apiGroup Contact
 *
 * @apiUse RemoveContact
 */
router.post(["/remove", "/block/remove"], async function(req, res) {
  const { portalId: targetPortalId } = req.body;
  const { device: userDevice } = req;

  try {
    const device = await Device.findDevice(targetPortalId);
    if (!device) {
      return res.json({status: 400, message: "Invalid Portal ID."});
    }

    await Contact.removeContact(userDevice.portalId, targetPortalId);

    return res.json({status: 200, message: "Success."});
  } catch (err) {
    console.log(err);
    return res.json({status: 500, message: "Internal server error."});
  }
});

/**
 * @api {get} /contact/search/:portalId Search for Contact
 * @apiName GetContactSearch
 * @apiGroup Contact
 *
 * @apiParam {String} portalId Desired contact's portalId.
 *
 * @apiSuccess {Number} status Status code.
 * @apiSuccess {String} message Status description.
 * @apiSuccess {Object} device Device Info
 * @apiSuccess {String} device.portalId Portal ID.
 * @apiSuccess {String} device.deviceName Device Name.
 * @apiSuccess {String} device.username User's Display Name.
 *
 * @apiError 400 User input error.
 * @apiError 401 User unauthorized
 * @apiError 499 Invalid Device
 * @apiError (Error 5xx) 500 Internal server error.
 */
router.get("/search/:portalId", async function(req, res) {
  const { portalId: targetPortalId } = req.params;
  const { device: userDevice } = req;

  try {
    const device = await Device.findDevice(targetPortalId);
    if (!device) {
      return res.json({status: 400, message: "Invalid portal ID."});
    }

    // do not return result if you are blocked
    const targetContact = await Contact.getContact(targetPortalId, userDevice.portalId);
    if (targetContact && targetContact.status === ContactStatus.blocked) {
      return res.json({status: 400, message: "Invalid portal ID."});
    }

    const deviceInfo = device.getPublicDeviceInfo();
    return res.json({status: 200, message: "Success.", device: deviceInfo});
  } catch (err) {
    console.log(err);
    return res.json({status: 500, message: "Internal server error."});
  }
});

export default router;