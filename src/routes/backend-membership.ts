import express from "express";
import moment from "moment";
import _capitalize from "lodash/capitalize";
import _findIndex from "lodash/findIndex";
import _keys from "lodash/keys";
import _map from "lodash/map";
import _isArray from "lodash/isArray";
import _isEmpty from "lodash/isEmpty";
import _isEqual from "lodash/isEqual";
import _mapValues from "lodash/mapValues";
import _merge from "lodash/merge";
import _pickBy from "lodash/pickBy";
import _reduce from "lodash/reduce";
import _values from "lodash/values";

import authBackendSession from "../middleware/backend-session";
import User from "../models/user";
import {
  MEMBERSHIP_STATUSES, ACCOUNT_STATUSES,
} from "../config/user.const";
import {
  PAGINATION_DEFAULT, PAGINATION_LIMIT, DATE_TIME_FORMAT, SORT_DIR, FLASH_TYPE,
} from "../config/backend.const";
import { getCount } from "../config/queries";
import Contact from "../models/contact";
import Device from "../models/device";
import { EMAIL_RE, PW_RE } from "../config/auth.regex";

const router = express.Router();

router.get("/", authBackendSession, async function (req, res) {
  try {
    return res.render("membership", {
      title: "Membership",
      DATE_TIME_FORMAT,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).render("error", {error});
  }
});

router.get("/data", authBackendSession, async function (req, res) {
  let { draw, start, length, order } = req.query;
  const { search } = req.query;
  try {
    // process query params
    draw = Number(draw);
    start = Number(start) || 0;
    length = Math.min(Number(length) || PAGINATION_DEFAULT, PAGINATION_LIMIT);

    /*** SORTING ***/
    order = _map(order, (o) => {
      let column;
      let dir;

      switch (Number(o.column)) {
        case 0: {
          column = "username";
          break;
        }
        case 1: {
          column = "email";
          break;
        }
        case 2: {
          column = "joinedTimestamp";
          break;
        }
        case 3: {
          column = "membershipStatus";
          break;
        }
        case 4: {
          column = "accountStatus";
          break;
        }
        default: {
          return null;
        }
      }

      switch (o.dir) {
        case SORT_DIR.desc: {
          dir = -1;
          break;
        }
        case SORT_DIR.asc: {
          dir = 1;
          break;
        }
        default: {
          return null;
        }
      }

      return _isArray(column)
        ? _reduce(column, (obj, col) => ({...obj, [col]: dir}), {})
        : { [column]: dir };
    });

    const hasSort = _isArray(order) && order[0] !== null;
    const mainDirection = hasSort
      ? _values(order[0])[0] || 1
      : 1;
    const sort = hasSort
      ? {
        ... order[0],
        _id: mainDirection, // used to stabilize sorts when all results are the same (related to PER-447)
      }
      : { joinedTimestamp: -1 };

    /*** SEARCHING & FILTERING ***/
    const baseFilter = [
    ];
    let filters = baseFilter;

    const searchString = String(search.value || "").trim();
    let searchDate;
    let searchDateFilter;
    try {
      searchDate = moment(searchString);

      if (searchDate.isValid()) {
        searchDateFilter = {
          $gte: new Date(moment(searchDate).startOf("day").format()),
          $lte: new Date(moment(searchDate).endOf("day").format()),
        };
      }
    } catch (_) {}

    let searchFilter;
    if (searchString !== "") {
      const defaultSearchObj = { $regex: searchString, $options: "i" };
      searchFilter = {
        $or: [
          { username: defaultSearchObj },
          { email: defaultSearchObj },
          { membershipStatus: defaultSearchObj },
          { accountStatus: defaultSearchObj },
          { "devices.deviceName": defaultSearchObj },
          { "devices.portalId": defaultSearchObj },
        ]
      };
      if (typeof searchDateFilter !== "undefined") {
        searchFilter["$or"] = [
          ...searchFilter["$or"],
          { joinedTimestamp: searchDateFilter },
        ];
      }

      filters = [
        ...filters,
        { $match: searchFilter },
      ];
    }

    // set up queries
    const aggregate = [
      { $lookup: {
        from: Device.collection.name,
        localField: "_id",
        foreignField: "userId",
        as: "devices",
      } },
      { $project: {
        verified: 1,
        username: 1,
        joinedTimestamp: 1,
        membershipStatus: 1,
        email: 1,
        accountStatus: 1,
        devices: 1,
      } }
    ];

    const processing = [
      { $sort: sort },
      { $limit: start + length },
      { $skip: start },
    ];

    // set up promises
    const countQuery = getCount();
    const usersPromise = User.aggregate([
      ...aggregate,
      ...filters,
      ...processing,
    ]);
    let filteredPromise;
    const totalPromise = User.aggregate([
      ...aggregate,
      ...baseFilter,
      ...countQuery
    ]);
    if (filters.length > baseFilter.length) {
      filteredPromise = User.aggregate([
        ...aggregate,
        ...filters,
        ...countQuery,
      ]);
    }

    // get results
    const [users, total, filtered] = await Promise.all([
      usersPromise,
      totalPromise,
      filteredPromise
    ]);

    const recordsTotal = total.length > 0 ? total[0].count : 0;
    const recordsFiltered = typeof filtered !== "undefined" && filtered.length > 0
      ? filtered[0].count
      : recordsTotal;

    const data = await Promise.all(users.map(async (user) => {
      const {
        _id, joinedTimestamp, username, email, verified,
      } = user;
      let {
        accountStatus, membershipStatus,
      } = user;

      const DT_RowId = _id.toString();
      const joinDate = moment(joinedTimestamp).format(DATE_TIME_FORMAT);
      accountStatus = verified ? _capitalize(accountStatus) : "Unverified";
      membershipStatus = _capitalize(membershipStatus);

      return {
        DT_RowId,
        username,
        email,
        joinDate,
        joinedTimestamp,
        accountStatus,
        membershipStatus,
      };
    }));

    return res.json({
      ... !isNaN(draw) && { draw },
      recordsTotal,
      recordsFiltered,
      data,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).render("secure-error", {error});
  }
});

router.get("/edit/:id", authBackendSession, async function (req, res) {
  const { id } = req.params;

  try {
    const [user, devices] = await Promise.all([
      User.findById(id, null, {lean: true}),
      Device.getDevices(id)
    ]);
    const formInput = req.flash("formInput")[0];
    if (formInput) {
      _merge(user, formInput);
    }
    user.id = user._id.toString();

    const { joinedTimestamp } = user;

    const joinDate = moment(joinedTimestamp).format(DATE_TIME_FORMAT);
    const accountStatuses = _map(ACCOUNT_STATUSES, (value, key) => ({
      key,
      value: _capitalize(value),
    }));

    return res.render("membership-edit", {
      title: `Manage User: ${user.username} ${!user.verified ? "(Unverified)" : ""}`,
      DATE_TIME_FORMAT,
      accountStatuses,
      joinDate,
      user,
      devices,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).render("error", {error});
  }
});

router.post("/edit", authBackendSession, async function (req, res) {
  const { id } = req.body;
  let {
    email, username, password, accountStatus,
  } = req.body;
  const messages = [];

  try {
    email = typeof email !== "undefined" ? email.trim() : "";
    password = typeof password !== "undefined" ? password.trim() : undefined;
    username = typeof username !== "undefined" ? username.trim() : "";
    accountStatus = typeof accountStatus !== "undefined" ? accountStatus.trim() : "";

    const passwordChanged = typeof password !== "undefined" && password !== "";

    if (!EMAIL_RE.test(email)) {
      messages.push("Please input valid email address.");
    }

    if (passwordChanged && !PW_RE.test(password)) {
      messages.push("Please input valid password.");
    }

    if (_isEmpty(username)) {
      messages.push("Please input valid user name.");
    }

    if (_findIndex(Object.values(ACCOUNT_STATUSES), (e) => (e === accountStatus)) < 0) {
      messages.push("Please select a valid account status.");
    }

    if (messages.length > 0) {
      req.flash(FLASH_TYPE.error, messages.join("\n"));
      req.flash("formInput", req.body);
      return res.redirect(`${req.baseUrl}/edit/${id}`);
    }

    // add everything back to data object
    // NOTE: password is handled separately
    const data = {
      email,
      username,
      accountStatus,
    };

    const oldUser = await User.findById(id, null, {lean: true});
    const difference: any = _reduce(_keys(data), function (result, key) {
      if ( !_isEqual(data[key], oldUser[key]) ) {
        result[key] = data[key];
      }
      return result;
    }, {});

    // shortcut if no changes detected
    if (!passwordChanged && _isEmpty(difference)) {
      req.flash(FLASH_TYPE.info, "No changes to save.");
      return res.redirect(`${req.baseUrl}/edit/${id}`);
    }

    // create final update
    const unset = _mapValues(_pickBy(difference, (v) => {
      return v === undefined || v === null;
    }), () => (1));
    const set = _pickBy(difference, (v) => {
      return v !== undefined && v !== null;
    });

    const update = {
      ... _keys(unset).length > 0 && {$unset: unset},
      ... _keys(set).length > 0 && {$set: set},
      ... passwordChanged && {password},
    };

    const newUser = await User.findOneAndUpdate({_id: oldUser._id}, update, {new: true});
    if (newUser) {
      // handle secondary updates if necessary
      if (difference.username) {
        await Device.updateDevicesForUser(newUser._id, { username: newUser.username });
      }

      req.flash(FLASH_TYPE.success, `User saved successfully!`);
      res.redirect(`${req.baseUrl}`);
    } else {
      req.flash(FLASH_TYPE.error, `Could not save user.`);
      req.flash("formInput", req.body);
      return res.redirect(`${req.baseUrl}/edit/${id}`);
    }
  } catch (error) {
    console.log(error);
    if (typeof error !== "undefined" && error.code === 11000) { // mongodb duplicate unique key code
      if (typeof error.message === "string" && error.message.includes("email")) {
        req.flash(FLASH_TYPE.error, `Email address already in use.`);
        req.flash("formInput", req.body);
        return res.redirect(`${req.baseUrl}/edit/${id}`);
      }
    }
    return res.status(500).render("error", {error});
  }
});

router.post("/device/delete", authBackendSession, async function (req, res) {
  const { id: portalId } = req.body;

  try {
    const isDeleted = await Device.removeDeviceByPortalId(portalId);

    if (isDeleted) {
      await Contact.removeDeviceContacts(portalId);
      req.flash(FLASH_TYPE.success, `Device (${portalId}) was deleted!`);
      return res.redirect(`${req.baseUrl}`);
    } else {
      req.flash(FLASH_TYPE.error, `Could not delete device ${portalId}.`);
      return res.redirect(`${req.baseUrl}`);
    }
  } catch (error) {
    console.log(error);
    return res.status(500).render("error", {error});
  }
});

export default router;