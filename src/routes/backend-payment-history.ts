import express from "express";
import authBackendSession from "../middleware/backend-session";

const router = express.Router();

router.get("/", authBackendSession, async function (req, res) {
  try {
    return res.render("payment-history", {
      title: "Payment History",
    });
  } catch (error) {
    console.log(error);
    return res.status(500).render("error", {error});
  }
});

export default router;